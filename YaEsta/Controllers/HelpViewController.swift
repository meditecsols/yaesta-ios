//
//  HelpViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 10/07/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit

class HelpTableViewCell: UITableViewCell {

    @IBOutlet weak var img_help: UIImageView!
    @IBOutlet weak var lbl_help_title: UILabel!
    
    
}

class HelpViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
 
    var help_list: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        help_list = ["Pedidos", "Pagos", "General","Factura"]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return help_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "help_cell", for: indexPath) as! HelpTableViewCell
        
        cell.lbl_help_title.text = help_list[indexPath.row]
        
        var image_name = ""
        
        switch indexPath.row {
        case 0:
            image_name="pedidos"
        case 1:
            image_name="pagos"
        case 2:
            image_name="general"
        case 3:
            image_name="factura"
        default:
            image_name="pedidos"
        }
        
        let imagehelpname = UIImage(named: image_name)
        
         cell.img_help.image = imagehelpname
        
        return cell
    }

 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "question_segue" {
            
        }
    }
 
    @IBAction func back_action(_ sender: Any){
    navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: false)
        self.performSegue(withIdentifier: "question_segue", sender: self)
    }
    
}
