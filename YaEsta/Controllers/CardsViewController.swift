//
//  CardsViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 02/07/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit

class CardsTableViewCell: UITableViewCell {
 
    @IBOutlet weak var img_card: UIImageView!
    @IBOutlet weak var lbl_number_card: UILabel!
    @IBOutlet weak var img_checked: UIImageView!
    
}

class CardsViewController: UIViewController,UITableViewDataSource, UITableViewDelegate  {
    
   
    @IBOutlet weak var tv_cards: UITableView!
    var cards_options: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        cards_options = ["····  5506", "····  4506", "····  8860", "····  9910"]
        
        
     
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
      /*  tv_cards.frame = CGRect(x: tv_cards.frame.origin.x, y: tv_cards.frame.origin.y, width: tv_cards.frame.size.width, height: tv_cards.contentSize.height+100)
        tv_cards.reloadData()*/
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards_options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cards_cell", for: indexPath) as! CardsTableViewCell
        var imageName = ""
        switch indexPath.row {
        case 0:
            imageName = "mastercard"
        default:
            imageName = "mastercard"
        }
        let imagecardname = UIImage(named: imageName)
        cell.img_card.image = imagecardname
        cell.lbl_number_card.text = cards_options[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    @IBAction func back_Action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
