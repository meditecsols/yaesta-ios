//
//  LoginViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 21/06/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginViewController: UIViewController {
    @IBOutlet weak var btn_ingresar: GradientButton!
    
    @IBOutlet weak var lbl_forgotpass: UILabel!
    
    @IBOutlet weak var lbl_register: UILabel!
    
    @IBOutlet weak var txt_email: UITextField!
    
    @IBOutlet weak var txt_password: UITextField!
    
    @IBOutlet var scrollContainer: UIScrollView!
    
     @IBOutlet var viewContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        let tapfor = UITapGestureRecognizer(target: self, action: #selector(tapForgot(gesture:)))
        lbl_forgotpass.addGestureRecognizer(tapfor)
        
        let tapregister = UITapGestureRecognizer(target: self, action: #selector(tapRegister(gesture:)))
        lbl_register.addGestureRecognizer(tapregister)
        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:)))
        self.viewContainer.isUserInteractionEnabled = true
        self.viewContainer.addGestureRecognizer(tap)
        //txt_email.text = "martingescamilla+1@gmail.com"
        //txt_password.text = "Albaes15"
    }
    @objc func handleTap(gesture: UITapGestureRecognizer) {
        self.view .endEditing(true)
    }
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPass(testStr:String) -> Bool {
        if testStr.count <= 0 {
            return false
        }
        return true
    }
    
    
    @objc func tapForgot(gesture: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "forgot_segue", sender: self)
    }
    
    @objc func tapRegister(gesture: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "register_segue", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_ingresar(_ sender: Any) {
        
        self.view .endEditing(true)
        if !isValidEmail(testStr: txt_email.text!) {
            SharedFunctions .sharedInstance().showAlert(withMessage: "Ingresa un correo electrónico válido.")
            return
        }
        if !isValidPass(testStr: txt_password.text!) {
            SharedFunctions .sharedInstance().showAlert(withMessage: "Ingresa una contraseña.")
            return
        }
        
        SVProgressHUD.show()
        let dict = NSMutableDictionary()
        dict["usr"]=txt_email.text
        dict["psw"]=txt_password.text
        let invoke = InvokeService()
        invoke .login(withCompletion: dict) { (response, error) in
            
            //SVProgressHUD.dismiss(completion: {
            
            if error != nil {
                SVProgressHUD.dismiss()
                SharedFunctions .sharedInstance().showAlert(withMessage: "Error al Iniciar Sesión.")
                return
            }
            
            
            if response?.count==0{
                SVProgressHUD.dismiss()
                SharedFunctions .sharedInstance().showAlert(withMessage: "Usuario o Contraseña incorrecto.")
                return
            }
            dict["key"] = response!["key"]
            dict["pk"] = response!["user"]
            dict["email"] = response!["user_email"]
            
            accountInfo = NSMutableDictionary(dictionary: SharedFunctions.sharedInstance().dictionaryByReplacingNulls(withStrings: dict))
            
            SVProgressHUD.dismiss(completion: {
                //userRetrievedInfo=NSMutableDictionary(dictionary: SharedFunctions.sharedInstance().dictionaryByReplacingNulls(withStrings:response))
                
                
                SharedFunctions.sharedInstance().updatePushToken(fcmTokenRetrieved as String?)
                
                if response!["customer_id"] is NSNull {
                    
                    self.performSegue(withIdentifier: "complete_profile_segue", sender: self)
                    
                }
                else
                {
                    
                    
                    
                    let invoke3 = InvokeService()
                    invoke3.getCustomerWithCompletion({ (response, error) in
                        
                        if error != nil {
                            DispatchQueue.main.async {
                                SharedFunctions .sharedInstance().showAlert(withMessage: "Error al Iniciar Sesión.")
                                return
                            }
                        }
                        
                        
                        if (response != nil)
                        {
                                let profile = response![0] as! NSDictionary
                                let detail = profile["detail"] as? String
                            if(detail == nil){
                                let profileMutableDict: NSMutableDictionary = NSMutableDictionary(dictionary: profile)
                                userRetrievedInfo=NSMutableDictionary(dictionary: SharedFunctions.sharedInstance().dictionaryByReplacingNulls(withStrings:profileMutableDict ))
                                SharedFunctions.sharedInstance().saveUserData(userRetrievedInfo! as! [AnyHashable : Any])
                                DispatchQueue.main.async {
                                    SharedFunctions.sharedInstance().saveUser(self.txt_email.text, andPass: self.txt_password.text)
                                    self.performSegue(withIdentifier: "list_shops_segue", sender: self)
                                }
                            }
                            else{
                                    SharedFunctions .sharedInstance().showAlert(withMessage: "Usuario o contraseña incorrecta")
                                    return
                                }
                            
                            

                        
                        }
                        
                        
                        
                    })
                    
                    
                }
                
            })
            
        }
        
        
    }

}
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "forgot_segue" {
            
        }
        else if segue.identifier == "register_segue"
        {
            
        }
        else if segue.identifier == "list_shops_segue"
        {
            
        }
        
    }



