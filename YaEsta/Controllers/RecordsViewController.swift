//
//  RecordsViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 09/07/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit
import SVProgressHUD
class RecordsTableViewCell: UITableViewCell {
  
    

    @IBOutlet weak var lbl_shop_title: UILabel!
    
    @IBOutlet weak var lbl_date_order: UILabel!
    
    @IBOutlet weak var lbl_numeric_code: UILabel!
    
    @IBOutlet weak var btn_details: GradientButton!
    
    @IBOutlet weak var lbl_cost: UILabel!
    
}


class RecordsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var records_list = NSMutableArray()
    
    @IBOutlet weak var tv_records: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
          //records_list = ["Capeltic", "La Terraza", "El Cubo"]
        getRecords()
        // Do any additional setup after loading the view.
    }

    func getRecords()  {
        SVProgressHUD.show()
        let invoke = InvokeService()
        invoke.getOrderWithCompletion { (response, error) in
            if error != nil
            {
                return
            }
            SVProgressHUD.dismiss(completion: {
                if (response?.count)! > 0
                {
                    self.records_list = response!
                    self.tv_records.reloadData()
                }
                else
                {
             
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "record_details_segue" {
            
        }
    }
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "recordcell", for: indexPath) as! RecordsTableViewCell
        
        let record = records_list[indexPath.row] as! NSDictionary
        
        cell.lbl_shop_title.text = "Orden #"+(String(record["id"] as! Int))
        let isoDate = record["created_at"] as! String
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let date = dateFormatter.date(from: isoDate)!
        
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
        let finaldate = dateFormatter.string(from: date)
        
        cell.lbl_date_order.text = finaldate
        
        let status = record["order_status"] as? String
        
        
        cell.lbl_numeric_code.text =  getStatusName(status: status!, cell: cell)
        
        cell.lbl_cost.text = "$" + (record["cost_base"] as! String)
    //cell.btn_details.addTarget(self, action: #selector(editButtonTapped), for: UIControlEvents.touchUpInside)
        
        return cell;
    }
    func getStatusName(status:String,cell:RecordsTableViewCell) -> String {
        var status_name = ""
        if status == "open"
        {
            status_name = "Nueva orden"
            cell.lbl_numeric_code.textColor = UIColor.blue
        }
        else if status == "ready"
        {
            cell.lbl_numeric_code.textColor = UIColor.green
            status_name = "Lista"
        }
        else if status == "cancelledbybusiness"
        {
            cell.lbl_numeric_code.textColor = UIColor.red
            status_name = "Cancelada"
        }
        else if status == "accepted"
        {
            cell.lbl_numeric_code.textColor = UIColor.orange
            status_name = "En progreso"
        }
        return status_name
    }
    @objc func editButtonTapped(button: UIButton) {
        
        
        self.performSegue(withIdentifier: "record_details_segue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 174
    }

    @IBAction func back_action(_ sender: Any) {
    navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
}
