//
//  SearchDetailsViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 19/07/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit


class SearchDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var img_product: UIImageView!
    @IBOutlet weak var lbl_product_name: UILabel!
    
    @IBOutlet weak var lbl_product_description: UILabel!
    @IBOutlet weak var lbl_product_cost: UILabel!
    
}

class SearchDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    var search_list: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        search_list    = ["Pizza Amore"]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return search_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "search_detail_cell", for: indexPath) as! SearchDetailsTableViewCell
        
        let productName = search_list[indexPath.row]
       
        cell.lbl_product_name.text = productName
        cell.lbl_product_description.text = "Italiana"
        cell.lbl_product_cost.text = "15-25 min"
         cell.img_product?.image = UIImage(named: "oval.png")
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    @IBAction func back_action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
}
