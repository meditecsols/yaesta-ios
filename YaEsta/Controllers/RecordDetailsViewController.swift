//
//  RecordDetailsViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 09/07/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit

class Record_DetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_product: UILabel!
    @IBOutlet weak var lbl_product_cost: UILabel!
    
}

class RecordDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var record_details_list: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        record_details_list = ["Espresso", "Sandwich de queso y pavo", "Sandwich de queso y pavo"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return record_details_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "record_detail_cell", for: indexPath) as! Record_DetailsTableViewCell
        
        cell.lbl_product.text = record_details_list[indexPath.row]
       
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
   
    @IBAction func back_action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
}
