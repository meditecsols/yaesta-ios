//
//  SearchViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 19/07/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit
class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var img_search: UIImageView!
    @IBOutlet weak var lbl_search: UILabel!
    
}
class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    var search_list: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        search_list    = ["Desayunos", "Pizzas", "Mexicana","Japonesa"]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return search_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "search_cell", for: indexPath) as! SearchTableViewCell
        
        cell.lbl_search.text = search_list[indexPath.row]
        
        var imageName = ""
        
        switch indexPath.row {
        case 0:
            imageName = "desayunos"
            break
        case 1:
            imageName = "pizza"
            break
        case 2:
            imageName = "mexicana"
            break
        case 3:
            imageName = "japonesa"
            break
            
        default:
            break
        }
         let imagesearch = UIImage(named: imageName)
         cell.img_search.image = imagesearch
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 173
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "segue_search_details", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_search_details" {
            
        }
    }


}
