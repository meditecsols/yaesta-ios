//
//  RegisterVC.h
//  YaEsta
//
//  Created by Martin Gonzalez on 16/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *txtFieldName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldPass;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldPassConfirmation;
@property (strong, nonatomic) IBOutlet UILabel *lblTerms;
-(IBAction)actionNext:(id)sender;
-(IBAction)actionBack:(id)sender;
@end
