//
//  ListShopVC.h
//  YaEsta
//
//  Created by Martin Gonzalez on 21/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListShopVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong,nonatomic) IBOutlet UITableView *tableShops;
@end
