//
//  RMCustomViewActionController.h

//
//  Created by Martin Gonzalez on 21/06/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <RMActionController/RMActionController.h>

@interface RMCustomViewActionController : RMActionController<UIView *>

@end
