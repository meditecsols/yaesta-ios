//
//  MyProfileVC.m
//  YaEsta
//
//  Created by Martin Gonzalez on 21/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import "MyProfileVC.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"
#import "InvokeService.h"
#import "SVProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MyProfileVC ()

@end

@implementation MyProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tap];
    _txtEmail.enabled = false;
}
-(void)viewWillAppear:(BOOL)animated{
    _txtName.text=[NSString stringWithFormat:@"%@" ,[userRetrievedInfo objectForKey:@"first_name"]];
    if ([[accountInfo objectForKey:@"usr"] length]>0) {
        _txtEmail.text=[accountInfo objectForKey:@"usr"];
        [_txtEmail setEnabled:NO];
    }
    [self readProfileImageSaved];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(captureImage)];
    [_imgProfile setUserInteractionEnabled:YES];
    [_imgProfile addGestureRecognizer:tap];
}
- (void) hideKeyboard{
    [self.view endEditing:YES];
}
-(void)readProfileImageSaved{
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getCustomerImageWithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        if ([urlImageUser length]>0) {
            [self->_imgProfile sd_setImageWithURL:[NSURL URLWithString:urlImageUser]
                           placeholderImage:nil
                                    options: SDWebImageCacheMemoryOnly
                                  completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                      if (image) {
                                          [self roundImageProfile];
                                      }
                                      else{
                                          [self.imgProfile setImage:[UIImage imageNamed:@"coffee.png"]];
                                      }
                                      
                                  }];
        }
    }];
    
}
-(void)roundImageProfile{
    if (_imgProfile.frame.size.height!=_imgProfile.frame.size.width) {
        [_imgProfile setFrame:CGRectMake(_imgProfile.frame.origin.x, _imgProfile.frame.origin.y, _imgProfile.frame.size.height, _imgProfile.frame.size.height)];
    }
    _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width / 2;
    _imgProfile.layer.borderWidth = 1.0f;
    _imgProfile.layer.borderColor = [UIColor blueColor].CGColor;
    _imgProfile.clipsToBounds = YES;
}
-(void)captureImage{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"Seleccione una opción" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Hacer foto" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            imagePicker.allowsEditing = NO;
            [self presentViewController:imagePicker animated:YES completion:nil];
            //newMedia = YES;
        }
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Seleccionar existente" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            imagePicker.allowsEditing = NO;
            [self presentViewController:imagePicker animated:YES completion:nil];
            //newMedia = NO;
        }
    }]];
    // Present action sheet.
    
    
    [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
    
    //For set action sheet to middle of view.
    CGRect rect = self.view.frame;
    rect.origin.x = self.view.frame.size.width / 20;
    rect.origin.y = self.view.frame.size.height / 20;
    actionSheet.popoverPresentationController.sourceView = self.view;
    actionSheet.popoverPresentationController.sourceRect = rect;
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width scaledToHeight:(float) i_height
{
    UIImage *newImage;
    if (sourceImage.size.width>i_width) {
        float oldWidth = sourceImage.size.width;
        float scaleFactor = i_width / oldWidth;
        
        float newHeight = sourceImage.size.height * scaleFactor;
        float newWidth = oldWidth * scaleFactor;
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    else{
        newImage=sourceImage;
    }
    
    if(newImage.size.height>i_height){
        float oldHeight = sourceImage.size.height;
        float scaleFactor = i_height / oldHeight;
        
        float newWidth = sourceImage.size.width * scaleFactor;
        float newHeight =oldHeight * scaleFactor;
        
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return newImage;
}
#pragma mark - Profile Image methods


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image =[self imageWithImage:[info objectForKey:UIImagePickerControllerOriginalImage] scaledToWidth:500 scaledToHeight:500] ;
    _imgProfile.image = image;
    [self roundImageProfile];
    //[btnImagePicker setBackgroundImage:imageViewBack.image forState:UIControlStateNormal];
    //imageViewBack.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSData *imageData = UIImageJPEGRepresentation(image, 0);
    NSError *error;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    documentsDirectory=[documentsDirectory stringByAppendingString:@"/ProfileInfo"];
    if (![fileMgr fileExistsAtPath:documentsDirectory]){
        [fileMgr createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    NSString *nameImage=[NSString stringWithFormat:@"%@.jpg",[userRetrievedInfo objectForKey:@"id"]];
    NSString *path= [documentsDirectory stringByAppendingPathComponent:nameImage];
    
    [imageData writeToFile:path options:NSDataWritingAtomic error:&error];
    
    NSMutableDictionary *dictForUpdate=[[NSMutableDictionary alloc] init];
    [dictForUpdate setObject:[userRetrievedInfo objectForKey:@"id"] forKey:@"profile"];
    [dictForUpdate setObject:path forKey:@"pathProfileImage"];
    
    if ([[dictForUpdate allKeys] count]>0) {
        InvokeService *invoke=[[InvokeService alloc] init];
        [SVProgressHUD show];
        [invoke updateCustomerImageWithData:dictForUpdate WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                if ([[responseData allKeys] count]>0) {
                    //[self showAlertWithMessage:@"Su imagen se guardó correctamente"];
                    [self readProfileImageSaved];
                }
                else{
                    [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al actualizar su imagen"];
                }
                
            });
            
        }];
    }
    
}

-(void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if(error){
        [[SharedFunctions sharedInstance] showAlertWithMessage:error.description];
        
    }
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)actionSave:(id)sender{
    [self.view endEditing:YES];
    if (_txtEmail.text.length>0&&![self validateEmailWithString:_txtEmail.text]) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar un Email válido"];
        return;
    }
    if (_txtName.text.length<=0 ) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar tu nombre"];
        return;
    }
    NSMutableDictionary *dictForUpdate=[[NSMutableDictionary alloc] init];
   /* if (![[userRetrievedInfo objectForKey:@"email"] isEqualToString:_txtEmail.text]) {
        [dictForUpdate setObject:_txtEmail.text forKey:@"email"];
    }*/
    if (![[userRetrievedInfo objectForKey:@"first_name"] isEqualToString:_txtName.text]) {
        [dictForUpdate setObject:_txtName.text forKey:@"first_name"];
    }
    if ([[dictForUpdate allKeys] count]>0) {
        InvokeService *invoke=[[InvokeService alloc] init];
        [SVProgressHUD show];
        [invoke updateCustomerWithData:dictForUpdate WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                if ([[responseData allKeys] count]>0) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else{
                    [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al actualizar sus datos"];
                }
                
            });
            
        }];
    }
}
- (IBAction)actionCloseSession:(id)sender{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"¿Desea cerrar su sesión?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* noAction = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Si"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [[SharedFunctions sharedInstance] clearUsrAndPass];
                                   UIStoryboard *storyboard = self.storyboard;
                                   UIViewController *next= [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                   next.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                   [self presentViewController:next animated:YES completion:nil];
                                   
                               }];
    [alert addAction:noAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)actionBack:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
