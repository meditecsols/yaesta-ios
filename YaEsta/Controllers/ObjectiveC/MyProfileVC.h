//
//  MyProfileVC.h
//  YaEsta
//
//  Created by Martin Gonzalez on 21/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>


@interface MyProfileVC : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;
- (IBAction)actionSave:(id)sender;
- (IBAction)actionCloseSession:(id)sender;
- (IBAction)actionBack:(id)sender;

@end
