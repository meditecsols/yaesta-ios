//
//  OrderVC.h
//  YaEsta
//
//  Created by Martin Gonzalez on 29/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong,nonatomic) IBOutlet UITableView *tableItems;
@property (strong, nonatomic) IBOutlet UILabel *lblNameShop;
@property (strong, nonatomic) IBOutlet UILabel *lblDescShop;
@property (strong, nonatomic) IBOutlet UILabel *lblSubTotal;
@property (strong, nonatomic) IBOutlet UILabel *lblShippmentCost;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalAmount;
@property (strong, nonatomic) IBOutlet UIImageView *imgCard;
@property (strong, nonatomic) IBOutlet UILabel *lblEndNumbCards;
@property (strong, nonatomic) IBOutlet UIButton *btnOrder;
@property (strong, nonatomic) IBOutlet UIButton *btnChangeCard;
- (IBAction)changeCard:(id)sender;
- (IBAction)actionCreateOrder:(id)sender;

@end
