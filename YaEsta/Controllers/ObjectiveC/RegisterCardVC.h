//
//  RegisterCardVC.h
//  YaEsta
//
//  Created by Martin Gonzalez on 22/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTHMonthYearPickerView.h"

@interface RegisterCardVC : UIViewController<UITextFieldDelegate,LTHMonthYearPickerViewDelegate>
@property BOOL comeFromAddCardList;
@property (nonatomic, strong) LTHMonthYearPickerView *monthYearPicker;
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtCard;
@property (strong, nonatomic) IBOutlet UITextField *txtDate;
@property (strong, nonatomic) IBOutlet UITextField *txtCode;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionNext:(id)sender;

@end
