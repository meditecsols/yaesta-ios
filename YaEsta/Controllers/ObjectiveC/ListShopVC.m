//
//  ListShopVC.m
//  YaEsta
//
//  Created by Martin Gonzalez on 21/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import "ListShopVC.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"
#import "InvokeService.h"
#import "SVProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>




@interface ListShopVC (){
    NSMutableArray *shopsArray;
    UIRefreshControl *refreshControl;
}

@end

@implementation ListShopVC

- (void)viewDidLoad {
    [super viewDidLoad];

    _tableShops.dataSource=self;
    _tableShops.delegate=self;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh1:) forControlEvents:UIControlEventValueChanged];
    [_tableShops addSubview:refreshControl];
    
    if (!comeFromLogin&&firstTimeEntry) {
        [[SharedFunctions sharedInstance] getUsrAndPass];
        InvokeService *invoke=[[InvokeService alloc] init];
        NSMutableDictionary *dict=[NSMutableDictionary new];
        [dict setObject:usr forKey:@"usr"];
        [dict setObject:psw forKey:@"psw"];
        
        [invoke loginWithCompletion:dict WithCompletion:^(NSMutableDictionary *responseData, NSError *error) {
            if ([[responseData objectForKey:@"key"] length]>0 && [[responseData objectForKey:@"key"] length]>0) {
                [dict setObject:[responseData objectForKey:@"key"] forKey:@"key"];
                [dict setObject:[responseData objectForKey:@"user"] forKey:@"pk"];
                [dict setObject:[responseData objectForKey:@"user_email"] forKey:@"email"];


                        accountInfo=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:dict]];
                        InvokeService *invoke2=[[InvokeService alloc] init];
                        [invoke2 getUserData:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                            if([[responseData allKeys] count]>0){
                                InvokeService *invoke3=[[InvokeService alloc] init];
                                [invoke3 getCustomerWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                                    if ([responseData isKindOfClass:[NSArray class]]) {
                                        NSMutableDictionary *dictionaryData = [NSMutableDictionary new];
                                        dictionaryData = [[responseData objectAtIndex:0] mutableCopy];
                                        userRetrievedInfo=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:dictionaryData]];
                                    }
                                    else if ([responseData isKindOfClass:[NSDictionary class]]){
                                        NSMutableDictionary *dictionaryData = [NSMutableDictionary new];
                                        dictionaryData = [responseData mutableCopy];
                                        userRetrievedInfo=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:dictionaryData]];
                                    }
                                    
                                    [[SharedFunctions sharedInstance] saveUserData:userRetrievedInfo];
                                    
                                    [self getShops];
                                    firstTimeEntry=NO;
                                    return;
                                    
                                }];
                            }
                        }];
               
            }
        }];
    }
    else
    {
         [self getShops];
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
}
- (void)refresh1:(UIRefreshControl *)refreshControl
{
    [refreshControl endRefreshing];
    [SVProgressHUD show];
    [self getShops];
    
}
-(void)getShops{
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getShopsByUserWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        if ([responseData isKindOfClass:[NSDictionary class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            return;
        }
        self->shopsArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]] mutableCopy];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->refreshControl endRefreshing];
            [SVProgressHUD dismiss];
            [self.tableShops reloadData];
        });
    }];
    return;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([shopsArray count]>0)
    {
        //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                = 1;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"Por el momento no hay \nningún restaurante disponible";
        noDataLabel.numberOfLines=4;
        noDataLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:20];
        noDataLabel.textColor        = [UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numOfSections;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return [shopsArray count];

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *)[self.tableShops dequeueReusableCellWithIdentifier:@"shopcell"];

    
    
    UIView *_view_shop = (UIView *)[cell viewWithTag:0];
    UIImageView *_img_shop= (UIImageView *)[cell viewWithTag:1];
    UILabel *_lbl_name_shop = (UILabel *)[cell viewWithTag:2];
    UILabel *_lbl_description_shop = (UILabel *)[cell viewWithTag:3];
    
    NSString *shopName = [[shopsArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    _lbl_name_shop.text = shopName;
    
    NSString *shopDesc = [[shopsArray objectAtIndex:indexPath.row] objectForKey:@"description"];
    _lbl_description_shop.text = shopDesc;
    //_lbl_type_shop.text = @"Cafetería";
    
    _view_shop.layer.cornerRadius=5;
    _view_shop.layer.masksToBounds=true;
    
    _view_shop.layer.shadowColor = [UIColor blackColor].CGColor;
    _view_shop.layer.shadowOffset = CGSizeMake(0, 1);
    _view_shop.layer.shadowOpacity = 0.2;
    _view_shop.layer.shadowRadius = 4.0;
            
    _img_shop.image = [UIImage imageNamed:@"coffee.png"];
            
            if ([[shopsArray objectAtIndex:indexPath.row] objectForKey:@"picture_set"]!= nil) {
                if ([[[shopsArray objectAtIndex:indexPath.row]  objectForKey:@"picture_set"] count] >0) {
                    NSDictionary *pictureset = [[[shopsArray objectAtIndex:indexPath.row]  objectForKey:@"picture_set"] objectAtIndex:0];
                    NSString *pictureurl = [pictureset objectForKey:@"picture"];
                    
                    [_img_shop sd_setImageWithURL:[NSURL URLWithString:pictureurl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if (image) {
                            [self roundImage:_img_shop];
                        }
                        else{
                            [_img_shop setImage:[UIImage imageNamed:@"coffee.png"]];
                        }
                        
                    }];
                }
                
            }
        

    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

      selectedShop = [shopsArray objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"shop_segue" sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}
-(void)roundImage:(UIImageView *)imgProfile{
    if (imgProfile.frame.size.height!=imgProfile.frame.size.width) {
        [imgProfile setFrame:CGRectMake(imgProfile.frame.origin.x, imgProfile.frame.origin.y, imgProfile.frame.size.height, imgProfile.frame.size.height)];
    }
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.layer.borderWidth = 0.5f;
    imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    imgProfile.clipsToBounds = YES;
}

@end
