//
//  RecoverPassVC.h
//  YaEsta
//
//  Created by Martin Gonzalez on 21/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecoverPassVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
- (IBAction)actionNext:(id)sender;
- (IBAction)actionBack:(id)sender;
@end
