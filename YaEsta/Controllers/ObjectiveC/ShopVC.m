//
//  ShopVC.m
//  YaEsta
//
//  Created by Arturo Escamilla on 27/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import "ShopVC.h"

@interface ShopVC ()
{
    NSMutableArray *productsArray;
     NSMutableArray *recomendedArray;
    UIRefreshControl *refreshControl;
}
@end

@implementation ShopVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
