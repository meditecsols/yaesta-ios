//
//  RegisterVC.m
//  YaEsta
//
//  Created by Martin Gonzalez on 16/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import "RegisterVC.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "SVProgressHUD.h"
#import "InvokeService.h"
#import "TermsVC.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface RegisterVC ()

@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(actionShowTerms)];
    
    [self.lblTerms addGestureRecognizer:tap2];
}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(BOOL)validatePassWithString:(NSString *)pass{
    if (pass.length>0) {
        return YES;
    }
    return NO;
}
-(IBAction)actionNext:(id)sender{
   /* if (_txtFieldName.text.length<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar tu Nombre"];
        return;
    }*/
    if (_txtFieldEmail.text.length<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar un Email"];
        return;
    }
    if (![self validateEmailWithString:_txtFieldEmail.text]) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar un Email válido"];
        return;
    }
    if (_txtFieldPass.text.length<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar una contraseña"];
        return;
    }
    
    if (_txtFieldPassConfirmation.text.length<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes confirmar tu contraseña"];
        return;
    }
    if (![_txtFieldPassConfirmation.text isEqualToString:_txtFieldPass.text]) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"La contraseña de confirmación no coincide"];
        return;
    }
    
    InvokeService *invoke=[[InvokeService alloc] init];
    NSMutableDictionary *dict=[NSMutableDictionary new];
    [dict setObject:_txtFieldEmail.text forKey:@"email"];
    [dict setObject:_txtFieldPass.text forKey:@"pass"];
    [dict setObject:_txtFieldName.text forKey:@"name"];
    
    [SVProgressHUD show];
    [invoke registerWithData:dict WithCompletion:^(NSMutableDictionary *responseData, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            
            if ([[responseData allKeys] count]<=2) {
                if ([responseData objectForKey:@"username"] ) {
                    if ([[responseData objectForKey:@"username"] isKindOfClass:[NSArray class]] ) {
                        if ([[responseData objectForKey:@"username"] count]>=1) {
                            if ([[[responseData objectForKey:@"username"] objectAtIndex:0] isEqualToString:@"A user with that username already exists."]) {
                                [[SharedFunctions sharedInstance] showAlertWithMessage:@"Este email ya ha sido registrado"];
                                return;
                            }
                        }
                    }
                }
                else if ([responseData objectForKey:@"detail"] )
                {
                    //[[SharedFunctions sharedInstance] saveUser:self->_txtFieldEmail.text andPass:self->_txtFieldPass.text];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        UIAlertController * alert=  [UIAlertController
                                                     alertControllerWithTitle:@"YaEstá!"
                                                     message:@"Para continuar con el registro abre el link que se te envió a tu correo electrónico"
                                                     preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        UIAlertAction* okAction = [UIAlertAction
                                                   actionWithTitle:@"Entendido"
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                                                   {
                                                       NSURL* mailURL = [NSURL URLWithString:@"message://"];
                                                       if ([[UIApplication sharedApplication] canOpenURL:mailURL]) {
                                                           [[UIApplication sharedApplication] openURL:mailURL options:@{} completionHandler:^(BOOL success) {
                                                               [self dismissViewControllerAnimated:YES completion:nil];
                                                           }];
                                                       }
                                                       
                                                   }];
                        
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:NO completion:nil];
                        // }];
                    });

                    
                }
                else if ([responseData objectForKey:@"password1"] )
                {
                        [[SharedFunctions sharedInstance] showAlertWithMessage:@"El password es muy corto favor de verificarlo"];
                    
                }
                else{
                    [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al Registrarse"];
                }
                
                
            }
            else{
                if ([[responseData allKeys] count]>5) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        UIAlertController * alert=  [UIAlertController
                                                     alertControllerWithTitle:@"YaEstá!"
                                                     message:@"Para continuar con el registro abre el link que se te envió a tu correo electrónico"
                                                     preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        UIAlertAction* okAction = [UIAlertAction
                                                   actionWithTitle:@"Entendido"
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                                                   {
                                                       NSURL* mailURL = [NSURL URLWithString:@"message://"];
                                                       if ([[UIApplication sharedApplication] canOpenURL:mailURL]) {
                                                           [[UIApplication sharedApplication] openURL:mailURL options:@{} completionHandler:^(BOOL success) {
                                                               [self dismissViewControllerAnimated:YES completion:nil];
                                                           }];
                                                       }
                                                       
                                                   }];
                        
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:NO completion:nil];
                        // }];
                    });
                }
            }
        });
    }];
    
}
-(IBAction)actionBack:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)actionShowTerms{
    InvokeService *invoke=[[InvokeService alloc] init];
    [SVProgressHUD show];
    [invoke getUrlsTermsPrivacyWithCompletion:^(NSMutableArray *responseData, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            if ([responseData count]>0) {
                
                UIStoryboard *storyboard = self.storyboard;
                TermsVC *terms= [storyboard instantiateViewControllerWithIdentifier:@"TermsVC"];
                terms.arrayUrls=responseData;
                [self presentViewController:terms animated:YES completion:^{
                    terms.lblTitle.text=@"Acerca de";
                }];
                
                
            }
            
            
            
        });
    }];
    
}

-(void)fblogin{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    if ([UIApplication.sharedApplication canOpenURL:[NSURL URLWithString:@"fb://"]])
    {
        login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    }
    
    [login logInWithReadPermissions:@[@"public_profile", @"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error)
        {
            NSLog(@"Unexpected login error: %@", error);
            NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: @"There was a problem logging in. Please try again later.";
            NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops";
            [[[UIAlertView alloc] initWithTitle:alertTitle
                                        message:alertMessage
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
        else
        {
            if(result.token)   // This means if There is current access token.
            {
                
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                                   parameters:@{@"fields": @"picture, name, email"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id userinfo, NSError *error) {
                     if (!error) {
                         
                         
                         dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                         dispatch_async(queue, ^(void) {
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 
                                 // you are authorised and can access user data from user info object
                                 
                             });
                         });
                         
                     }
                     else{
                         
                         NSLog(@"%@", [error localizedDescription]);
                     }
                 }];
            }
            NSLog(@"Login Cancel");
        }
    }];
}
@end
