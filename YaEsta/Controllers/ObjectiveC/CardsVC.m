//
//  CardsVC.m
//  YaEsta
//
//  Created by Martin Gonzalez on 22/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import "CardsVC.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"
#import "InvokeService.h"
#import "RMCustomViewActionController.h"
#import "SVProgressHUD.h"
#import "RegisterCardVC.h"


#define HEIGHT_ROW 80

@interface CardsVC (){
    NSMutableArray *arrayCards;
}

@end

@implementation CardsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrayCards=[[NSMutableArray alloc] init];
    _tableCards.delegate=self;
    _tableCards.dataSource=self;
    _layoutConstraintHeightTable.constant=0;
}
-(void)viewDidAppear:(BOOL)animated{
    [self getCards];
}
-(void)getCards{
    InvokeService *invoke=[[InvokeService alloc] init];
    [SVProgressHUD show];
        [invoke getCardsWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss]; //WithCompletition:^{
                if (!error) {
                    if ([responseData isKindOfClass:[NSArray class]]) {
                        if ([responseData count]>0) {
                            self->arrayCards=responseData;
                            [self.tableCards reloadData];
                            [self resizeTable];
                        }
                    }
                }
                //}];
                
            });
            
        }];
    ;
    
}
-(void)resizeTable{
    
    _layoutConstraintHeightTable.constant=HEIGHT_ROW *[arrayCards count];
    _layoutConstraintHeightViewContainer.constant= _layoutConstraintHeightTable.constant + 60;
    _tableCards.scrollEnabled=NO;
    
    
    [self.view layoutSubviews];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger numOfRows = 0;
    if ([arrayCards count]>0)
    {
        //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfRows                = [arrayCards count];
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"Por el momento no hay \nninguna tarjeta registrada";
        noDataLabel.numberOfLines=4;
        noDataLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:20];
        noDataLabel.textColor        = [UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numOfRows;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return HEIGHT_ROW;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *)[self.tableCards dequeueReusableCellWithIdentifier:@"cards_cell"];
    UIImageView *imageBrand = (UIImageView *)[cell viewWithTag:1];
    //UILabel *lblNameBank = (UILabel *)[cell viewWithTag:2];
    UILabel *lblEndDigits= (UILabel *)[cell viewWithTag:3];
    
    UIImageView *selectCard = (UIImageView *)[cell viewWithTag:4];
    
    // lblNameBank.text=@"Banamex/";
    if (![[[arrayCards objectAtIndex:indexPath.row] objectForKey:@"lastdigits_card"] isKindOfClass:[NSNull class]]) {
        lblEndDigits.text=[[arrayCards objectAtIndex:indexPath.row] objectForKey:@"lastdigits_card"];
    }
    if (![[[arrayCards objectAtIndex:indexPath.row] objectForKey:@"card_type"] isKindOfClass:[NSNull class]]) {
        if ([[[[arrayCards objectAtIndex:indexPath.row] objectForKey:@"card_type"] objectForKey:@"name"] isEqualToString:@"VISA"]) {
            [imageBrand setImage:[UIImage imageNamed:@"cardVisa"]];
        }
        else if ([[[[arrayCards objectAtIndex:indexPath.row] objectForKey:@"card_type"] objectForKey:@"name"] isEqualToString:@"AMERICAN_EXPRESS"]) {
            [imageBrand setImage:[UIImage imageNamed:@"cardAmex"]];
        }
        else{
            [imageBrand setImage:[UIImage imageNamed:@"cardMC"]];
        }
    }
    
    
    if(![[[arrayCards objectAtIndex:indexPath.row] objectForKey:@"default"] isKindOfClass:[NSNull class]] ){
        if ([[[arrayCards objectAtIndex:indexPath.row] objectForKey:@"default"] boolValue]) {
            selectCard.image=[UIImage imageNamed:@"check"];
        }
        else{
            selectCard.image=nil;
        }
    }
    else{
        selectCard.image=nil;
    }
    
    
    //cardVisa cardMC cardAmex palomita
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    RMActionControllerStyle style = RMActionControllerStyleWhite;
    RMCustomViewActionController *actionController = [RMCustomViewActionController actionControllerWithStyle:style];
    
    
    RMAction *predetermidaAction = [RMAction<UIView *> actionWithTitle:@"Establecer como predeterminada"  style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
        
        [actionController dismissViewControllerAnimated:YES completion:^{
            InvokeService *invoke=[[InvokeService alloc] init];
            [SVProgressHUD show];
                NSMutableDictionary *dictCard=[[NSMutableDictionary alloc] init];
                [dictCard setObject:[[self->arrayCards objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"id"];
                [dictCard setObject:@YES forKey:@"default"];
                [invoke updateCardWithData:dictCard WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                            if (!error) {
                                if ([responseData isKindOfClass:[NSDictionary class]]) {
                                    if ([[responseData objectForKey:@"detail"] isEqualToString:@"No encontrado."]) {
                                        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al establecer como predeterminada tu tarjeta"];
                                    }
                                    else{
                                        [self getCards];
                                    }
                                }
                                
                                
                            }
                            else{
                                [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al eliminar tu tarjeta"];
                            }
                        
                        
                        
                        
                    });
                    
                    
                }];
            
            
        }];
    }];
    
    RMAction *deleteAction = [RMAction<UIView *> actionWithTitle:@"Eliminar Tarjeta" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
        
        [actionController dismissViewControllerAnimated:YES completion:^{
            if ([self->arrayCards count]==1) {
                [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debe existir al menos una tarjeta"];
                return;
            }
            InvokeService *invoke=[[InvokeService alloc] init];
            [SVProgressHUD show];
                [invoke deleteCard:[[self->arrayCards objectAtIndex:indexPath.row]objectForKey:@"id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                            if (!error) {
                                if ([responseData isKindOfClass:[NSDictionary class]]) {
                                    if ([[responseData objectForKey:@"detail"] isEqualToString:@"No encontrado."]) {
                                        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al eliminar tu tarjeta"];
                                    }
                                    
                                }
                                else{
                                    [self getCards];
                                }
                                
                            }
                            else{
                                [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al eliminar tu tarjeta"];
                            }
                        
                        
                        
                        
                    });
                    
                    
                }];
            
            
        }];
        
        
        
    }];
    
    RMAction *cancelAction = [RMAction<UIView *> actionWithTitle:@"Cancelar" style:RMActionStyleCancel andHandler:^(RMActionController<UIView *> *controller) {
        NSLog(@"Action controller was canceled");
    }];
    
    
    actionController.title = @"";
    actionController.message = @"";
    
    [actionController addAction:cancelAction];
    [actionController addAction:deleteAction];
    [actionController addAction:predetermidaAction];
    
    [self presentViewController:actionController animated:YES completion:nil];
    
}

- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionAddCard:(id)sender {
    UIStoryboard *storyboard = self.storyboard;
    RegisterCardVC *vc= [storyboard instantiateViewControllerWithIdentifier:@"RegisterCardVC"];
    vc.comeFromAddCardList=YES;
    [self presentViewController:vc animated:YES completion:nil];
}

@end
