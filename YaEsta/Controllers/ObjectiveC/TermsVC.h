//
//  TermsVC.h
//  YaEsta
//
//  Created by Martin Gonzalez on 17/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface TermsVC : UIViewController<WKNavigationDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIView *webContainerView;
@property (strong, nonatomic) IBOutlet WKWebView *webView;
@property (strong, nonatomic) IBOutlet UIButton *btnTerms;
@property (strong, nonatomic) IBOutlet UIButton *btnPrivacity;
@property (strong, nonatomic) NSMutableArray *arrayUrls;
- (IBAction)closeView:(id)sender;
- (IBAction)actionOpenTerms:(id)sender;
- (IBAction)actionOpenPrivacity:(id)sender;
@end
