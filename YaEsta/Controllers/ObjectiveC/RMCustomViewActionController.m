//
//  RMCustomViewActionController.m

//
//  Created by Martin Gonzalez on 21/06/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import "RMCustomViewActionController.h"

@interface RMCustomViewActionController ()

@end

@implementation RMCustomViewActionController
- (instancetype)initWithStyle:(RMActionControllerStyle)aStyle title:(NSString *)aTitle message:(NSString *)aMessage selectAction:(RMAction *)selectAction andCancelAction:(RMAction *)cancelAction {
    self = [super initWithStyle:aStyle title:aTitle message:aMessage selectAction:selectAction andCancelAction:cancelAction];
    if(self) {
        self.contentView = [[UIView alloc] initWithFrame:CGRectZero];
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;

    }
    return self;
}

@end
