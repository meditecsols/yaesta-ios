//
//  CardsVC.h
//  YaEsta
//
//  Created by Martin Gonzalez on 22/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardsVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollCards;
@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (strong, nonatomic) IBOutlet UITableView *tableCards;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightTable;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightViewContainer;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionAddCard:(id)sender;

@end
