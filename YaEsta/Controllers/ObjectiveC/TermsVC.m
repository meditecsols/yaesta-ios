//
//  TermsVC.m
//  YaEsta
//
//  Created by Martin Gonzalez on 17/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import "TermsVC.h"

@interface TermsVC ()

@end

@implementation TermsVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
}

-(void)loadUrlWithUrlString:(NSString *)url andTitle:(NSString *)title{
    _lblTitle.text=title;
    _webView = [[WKWebView alloc] init];
    [_webView setFrame:CGRectMake(0, 0, _webContainerView.frame.size.width, _webContainerView.frame.size.height)];
    _webView.navigationDelegate = self;
    
    NSURL *nsurl=[NSURL URLWithString:url];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [_webView loadRequest:nsrequest];
    [self.webContainerView addSubview:_webView];
}
- (IBAction)closeView:(id)sender {
    if (![_btnPrivacity isHidden]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        [_btnPrivacity setHidden:NO];
        [_btnTerms setHidden:NO];
        [_webView removeFromSuperview];
        _lblTitle.text=@"Acerca de";
    }
    
}

- (IBAction)actionOpenTerms:(id)sender {
    for (NSDictionary *dict in _arrayUrls) {
        if ([[dict objectForKey:@"label"] isEqualToString:@"terminos"]) {
            NSString *url=[dict objectForKey:@"url"];
            if ([url length]>0) {
                [self loadUrlWithUrlString:url andTitle:@"Términos y Condiciones"];
                [_btnPrivacity setHidden:YES];
                [_btnTerms setHidden:YES];
            }
        }
    }
    
    
    
}

- (IBAction)actionOpenPrivacity:(id)sender {
    for (NSDictionary *dict in _arrayUrls) {
        if ([[dict objectForKey:@"label"] isEqualToString:@"segunda"]) {
            NSString *url=[dict objectForKey:@"url"];
            if ([url length]>0) {
                [self loadUrlWithUrlString:url andTitle:@"Aviso de Privacidad"];
                [_btnPrivacity setHidden:YES];
                [_btnTerms setHidden:YES];
            }
        }
    }
}

@end
