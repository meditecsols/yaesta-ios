//
//  RegisterCardVC.m
//  YaEsta
//
//  Created by Martin Gonzalez on 22/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import "RegisterCardVC.h"
#import "Config.h"
#import "Conekta.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "InvokeService.h"
#import "Config.h"
#import "SVProgressHUD.h"
#import "Luhn.h"


@interface RegisterCardVC (){
    NSString *previousTextFieldContent;
    UITextRange *previousSelection;
    //Openpay *openpay;
    NSString *sessionId;
}

@end

@implementation RegisterCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _txtCard.delegate= self;
    [_txtCard addTarget:self
                 action:@selector(reformatAsCardNumber:)
       forControlEvents:UIControlEventEditingChanged];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"MM / yyyy"];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSDate *minDate = [dateFormatter dateFromString:[NSString stringWithFormat: @"%li / %li", (long)month, (long)year]];
    NSDate *maxDate = [dateFormatter dateFromString:[NSString stringWithFormat: @"%i / %li", 12, year+5]];
    _monthYearPicker = [[LTHMonthYearPickerView alloc] initWithDate: [NSDate date]
                                                        shortMonths: NO
                                                     numberedMonths: YES
                                                         andToolbar: YES
                                                            minDate: minDate
                                                         andMaxDate: maxDate];
    _monthYearPicker.delegate = self;
    _txtDate.delegate = self;
    _txtDate.inputView=_monthYearPicker;
    _txtCode.delegate=self;
    
}
-(void)viewWillAppear:(BOOL)animated{
}
-(void)dismissKeyboard{
    [self.view endEditing:YES];
}
- (IBAction)actionBack:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)actionNext:(id)sender{
    [self dismissKeyboard];
    if ([_txtName.text length]<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar el Nombre del Titular de la tarjeta"];
        return;
    }
    if (![Luhn validateString:_txtCard.text]) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar un número válido de tarjeta"];
        return;
    }
    
    if ([_txtDate.text length]<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar la fecha de expiración de la tarjeta"];
        return;
    }
    if ([_txtCode.text length]<3||[_txtCode.text length]>4) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar el código CVC de la tarjeta"];
        return;
    }
    
    
    OLCreditCardType cardType = [Luhn typeFromString:_txtCard.text];
    NSString *brandCard=@"";
    if (cardType==OLCreditCardTypeAmex) {
        brandCard=@"amex";
    }
    else if (cardType==OLCreditCardTypeVisa) {
        brandCard=@"visa";
    }
    else if (cardType==OLCreditCardTypeMastercard) {
        brandCard=@"masterCard";
    }
    
    NSString *nombreCard=_txtName.text;
    NSString *numberCard=[_txtCard.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *yearCard=[[_txtDate.text componentsSeparatedByString:@"/"] objectAtIndex:1];
    NSString *monthCard=[[_txtDate.text componentsSeparatedByString:@"/"] objectAtIndex:0];
    NSString *cvcCard =_txtCode.text;
    
    [SVProgressHUD show];
    
    Conekta *conekta = [[Conekta alloc] init];
    
    [conekta setDelegate: self];
    
    [conekta setPublicKey:CONEKTA_PUBLIC_KEY];
    
    [conekta collectDevice];
    
    Card *card = [conekta.Card initWithNumber:numberCard name:nombreCard cvc: cvcCard expMonth: monthCard expYear:yearCard];
    
    Token *token = [conekta.Token initWithCard:card];
    
    [token createWithSuccess: ^(NSDictionary *data) {
        NSLog(@"Data: %@", data);
        NSMutableDictionary *dictCard=[[NSMutableDictionary alloc] init];
        [dictCard setObject:[data objectForKey:@"id"] forKey:@"token"];
        // [dictCard setObject:CONEKTA_PUBLIC_KEY forKey:@"key"];
        [dictCard setObject:[userRetrievedInfo objectForKey:@"id"] forKey:@"customer"];
        
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke addCardWithData:dictCard WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            [SVProgressHUD dismiss];
                if (!error) {
                    if ([[responseData allKeys]count]>0) {
                        UIAlertController * alert=  [UIAlertController
                                                     alertControllerWithTitle:@""
                                                     message:@"Se ha almacenado la tarjeta exitosamente"
                                                     preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        UIAlertAction* okAction = [UIAlertAction
                                                   actionWithTitle:@"Entendido"
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                                                   {
                                                       [self dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                       
                                                       
                                                   }];
                        
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:NO completion:nil];
                        
                        
                    }
                    else{
                        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al registrar la tarjeta."];
                    }
                }
                else{
                    [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al registrar la tarjeta."];
                }
           
            
        }];
    } andError: ^(NSError *error) {
        [SVProgressHUD dismiss];
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al registrar la tarjeta."];
    }];
}
-(void)reformatAsCardNumber:(UITextField *)textField
{
    NSUInteger targetCursorPosition =
    [textField offsetFromPosition:textField.beginningOfDocument
                       toPosition:textField.selectedTextRange.start];
    
    NSString *cardNumberWithoutSpaces =
    [self removeNonDigits:textField.text
andPreserveCursorPosition:&targetCursorPosition];
    
    if ([cardNumberWithoutSpaces length] > 16) {
        [textField setText:previousTextFieldContent];
        textField.selectedTextRange = previousSelection;
        return;
    }
    
    NSString *cardNumberWithSpaces =
    [self insertSpacesEveryFourDigitsIntoString:cardNumberWithoutSpaces
                      andPreserveCursorPosition:&targetCursorPosition];
    
    textField.text = cardNumberWithSpaces;
    UITextPosition *targetPosition =
    [textField positionFromPosition:[textField beginningOfDocument]
                             offset:targetCursorPosition];
    
    [textField setSelectedTextRange:
     [textField textRangeFromPosition:targetPosition
                           toPosition:targetPosition]
     ];
}

-(BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    BOOL cambiar=YES;
    if ([textField isEqual:_txtCard]) {
        previousTextFieldContent = textField.text;
        previousSelection = textField.selectedTextRange;
    }
    else if([textField isEqual:_txtCode]){
        if (textField.text.length > 3 && range.length == 0)
        {
            cambiar=NO;
        }
        else
        {
            
            NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
            cambiar= [string isEqualToString:filtered];
            if (textField.text.length==3&&![string isEqualToString:@""]&&cambiar) {
                textField.text=[NSString stringWithFormat:@"%@%@",textField.text,string];
                [textField resignFirstResponder];
                
            }
            
            
        }
    }
    
    
    return cambiar;
}
- (NSString *)removeNonDigits:(NSString *)string
    andPreserveCursorPosition:(NSUInteger *)cursorPosition
{
    NSUInteger originalCursorPosition = *cursorPosition;
    NSMutableString *digitsOnlyString = [NSMutableString new];
    for (NSUInteger i=0; i<[string length]; i++) {
        unichar characterToAdd = [string characterAtIndex:i];
        if (isdigit(characterToAdd)) {
            NSString *stringToAdd =
            [NSString stringWithCharacters:&characterToAdd
                                    length:1];
            
            [digitsOnlyString appendString:stringToAdd];
        }
        else {
            if (i < originalCursorPosition) {
                (*cursorPosition)--;
            }
        }
    }
    
    return digitsOnlyString;
}
- (NSString *)insertSpacesEveryFourDigitsIntoString:(NSString *)string
                          andPreserveCursorPosition:(NSUInteger *)cursorPosition
{
    NSMutableString *stringWithAddedSpaces = [NSMutableString new];
    NSUInteger cursorPositionInSpacelessString = *cursorPosition;
    for (NSUInteger i=0; i<[string length]; i++) {
        if ((i>0) && ((i % 4) == 0)) {
            [stringWithAddedSpaces appendString:@" "];
            if (i < cursorPositionInSpacelessString) {
                (*cursorPosition)++;
            }
        }
        unichar characterToAdd = [string characterAtIndex:i];
        NSString *stringToAdd =
        [NSString stringWithCharacters:&characterToAdd length:1];
        
        [stringWithAddedSpaces appendString:stringToAdd];
    }
    
    return stringWithAddedSpaces;
}
#pragma mark - LTHMonthYearPickerView Delegate
- (void)pickerDidPressCancelWithInitialValues:(NSDictionary *)initialValues {
    _txtDate.text =[NSString stringWithFormat:@"%@/%@",[self formatMonth:[NSString stringWithFormat:@"%@",initialValues[@"month"]]],[self formatYear: [NSString stringWithFormat:@"%@",initialValues[@"year"]]]];
    [_txtDate resignFirstResponder];
    [_txtCode becomeFirstResponder];
}


- (void)pickerDidPressDoneWithMonth:(NSString *)month andYear:(NSString *)year {
    _txtDate.text =[NSString stringWithFormat:@"%@/%@",month,[year substringFromIndex:2]];
    [_txtDate resignFirstResponder];
    [_txtCode becomeFirstResponder];
}
-(NSString *)formatMonth:(NSString *)month{
    NSString *monthFormatted=month;
    if ([month intValue]<=0||[month intValue]>12) {
        return @"";
    }
    if ([month length]==1) {
        monthFormatted=[NSString stringWithFormat:@"0%@",month];
        
    }
    return monthFormatted;
}
-(NSString *)formatYear:(NSString *)year{
    NSString *yearFormatted=year;
    if ([year intValue]<=0||[year length]>4) {
        return @"";
    }
    if ([year length]==4) {
        if ([[year substringToIndex:2] isEqualToString:@"20"]) {
            yearFormatted=[year substringFromIndex:2];
        }
    }
    return yearFormatted;
}
-(NSString *)formatCreditCard:(NSString *)creditCard{
    NSString *creditCardFormatted=creditCard;
    if ([creditCard intValue]<=0||[creditCard length]>16) {
        return @"";
    }
    if ([creditCard length]==4) {
        if ([[creditCard substringToIndex:2] isEqualToString:@"20"]) {
            creditCardFormatted=[creditCard substringFromIndex:2];
        }
    }
    return creditCardFormatted;
}
- (void)pickerDidPressCancel {
    [_txtDate resignFirstResponder];
}

@end
