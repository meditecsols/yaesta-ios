//
//  ShopVC.h
//  YaEsta
//
//  Created by Arturo Escamilla on 27/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource>

@end
