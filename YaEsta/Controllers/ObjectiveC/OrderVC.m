//
//  OrderVC.m
//  YaEsta
//
//  Created by Martin Gonzalez on 29/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import "OrderVC.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"
#import "InvokeService.h"
#import "SVProgressHUD.h"

@interface OrderVC (){
    NSMutableArray *itemsArray;
    NSMutableArray *arrayCards;
    UIView *viewnoItems;
    NSMutableDictionary *dictOrderCreated;
}

@end

@implementation OrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableItems.dataSource=self;
    _tableItems.delegate=self;
    itemsArray=[[NSMutableArray alloc] initWithArray:[myCart objectForKey:@"itemsList"]];
    self->_lblEndNumbCards.text=@"Para terminar la orden debes registrar al menos una tarjeta";
    //[self->_btnOrder setEnabled:NO];
    self->_imgCard.hidden=YES;
    viewnoItems = [[UIView alloc] initWithFrame:self.view.frame];
}
-(void)viewDidAppear:(BOOL)animated{
    itemsArray=[[NSMutableArray alloc] initWithArray:[myCart objectForKey:@"itemsList"]];
    [self verifyIfItems];
    [self getCards];
}
-(void)getCards{
    InvokeService *invoke=[[InvokeService alloc] init];
    [SVProgressHUD show];
    [invoke getCardsWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss]; //WithCompletition:^{
            if (!error) {
                if ([responseData isKindOfClass:[NSArray class]]) {
                    if ([responseData count]>0) {
                        self->arrayCards=responseData;
                        for (NSDictionary *dictCard in self->arrayCards) {
                            if(![[dictCard objectForKey:@"default"] isKindOfClass:[NSNull class]] ){
                                
                                if ([[dictCard objectForKey:@"default"] boolValue]) {
                                    if (![[dictCard objectForKey:@"card_type"] isKindOfClass:[NSNull class]]) {
                                        if (![[dictCard objectForKey:@"lastdigits_card"] isKindOfClass:[NSNull class]]) {
                                            self->_lblEndNumbCards.text=[dictCard objectForKey:@"lastdigits_card"];
                                        }
                                        if ([[[dictCard objectForKey:@"card_type"] objectForKey:@"name"] isEqualToString:@"VISA"]) {
                                            [self->_imgCard setImage:[UIImage imageNamed:@"cardVisa"]];
                                        }
                                        else if ([[[dictCard objectForKey:@"card_type"] objectForKey:@"name"] isEqualToString:@"AMERICAN_EXPRESS"]) {
                                            [self->_imgCard setImage:[UIImage imageNamed:@"cardAmex"]];
                                        }
                                        else{
                                            [self->_imgCard setImage:[UIImage imageNamed:@"cardMC"]];
                                        }
                                         [self->_btnOrder setEnabled:YES];
                                        self->_imgCard.hidden=NO;
                                    }
                                }
                            }
                            
                        }
                    }
                    else{
                        self->_lblEndNumbCards.text=@"Para terminar la orden debes registrar al menos una tarjeta";
                       // [self->_btnOrder setEnabled:NO];
                        self->_imgCard.hidden=YES;
                        self->_btnChangeCard.hidden=YES;
                        
                    }
                }
            }
            //}];
            
        });
        
    }];
    ;
    
}
-(void)verifyIfItems{
    if ([itemsArray count]==0) {
        [self showNoItemsView];
    }else{
        [viewnoItems removeFromSuperview];
        _lblNameShop.text=[[myCart objectForKey:@"businessInfo"] objectForKey:@"name"];
        _lblDescShop.text=[[myCart objectForKey:@"businessInfo"] objectForKey:@"description"];
        float subtotal=0;
        for (int i=0;i<[[myCart objectForKey:@"itemsList"] count]; i++) {
            subtotal = subtotal + [[[[myCart objectForKey:@"itemsList"] objectAtIndex:i] objectForKey:@"price"] floatValue];
        }
        _lblSubTotal.text=[NSString stringWithFormat:@"$%.02f",subtotal];
        _lblTotalAmount.text=[NSString stringWithFormat:@"$%.02f",subtotal];
        //_lblEndNumbCards.text=
        [_tableItems reloadData];
    }
}
-(void)showNoItemsView{
    
    [viewnoItems setBackgroundColor:[UIColor whiteColor]];
    UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    noDataLabel.text             = @"No hay nada en tu orden";
    noDataLabel.numberOfLines=4;
    noDataLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:20];
    noDataLabel.textColor        = [UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    noDataLabel.center=viewnoItems.center;
    [viewnoItems addSubview:noDataLabel];
    [self.view addSubview:viewnoItems];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;

    numOfSections                = 1;
    tableView.backgroundView = nil;
 

    
    return numOfSections;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [itemsArray count];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[SharedFunctions sharedInstance] deleteItemToCartAtIndex:(int)indexPath.row];
        [itemsArray removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
        [self verifyIfItems];
    }
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Eliminar";
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *)[self.tableItems dequeueReusableCellWithIdentifier:@"myorder_cell"];
    
    
    
    UILabel *_lbl_name_item = (UILabel *)[cell viewWithTag:1];
    UILabel *_lbl_price = (UILabel *)[cell viewWithTag:2];
    
    NSString *itemName = [[itemsArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    _lbl_name_item.text = itemName;
    
    float itemPrice = [[[itemsArray objectAtIndex:indexPath.row] objectForKey:@"price"] floatValue];
    _lbl_price.text = [NSString stringWithFormat:@"$%.02f",itemPrice];

    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}


- (IBAction)changeCard:(id)sender {
}
- (IBAction)actionCreateOrder:(id)sender{
    if ([arrayCards count]<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes agregar al menos una tarjeta de crédito o débito."];
        return;
    }
    NSMutableDictionary *dictOrder=[[NSMutableDictionary alloc] init];
    NSString *amountString = _lblTotalAmount.text;
    amountString = [amountString stringByReplacingOccurrencesOfString:@"$" withString:@""];
   // NSString *amount = [NSString stringWithFormat:@"%f",[amountString doubleValue]];
    [dictOrder setObject:[NSNumber numberWithFloat:[amountString floatValue]] forKey:@"cost_base"];
    [dictOrder setObject:[userRetrievedInfo objectForKey:@"id"] forKey:@"customer"];
    [dictOrder setObject:[NSNumber numberWithFloat:[[myCart objectForKey:@"business"] floatValue]] forKey:@"business"];
    [SVProgressHUD show];
    dictOrderCreated=[[NSMutableDictionary alloc] init];
    [self performAndWait:^(dispatch_semaphore_t semaphore) {
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke addOrderWithData:dictOrder WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        if ([[responseData allKeys] count]>3) {
            self->dictOrderCreated=[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:[responseData copy]];
            NSLog(@"order created %@",[self->dictOrderCreated objectForKey:@"id"]);
        }
         dispatch_semaphore_signal(semaphore);
    }];
    }];
    [self performAndWait:^(dispatch_semaphore_t semaphore) {
        if ([[self->dictOrderCreated allKeys] count]>3) {
            for ( int i=0;i<[[myCart objectForKey:@"itemsList"]count];i++) {
                [self performAndWait2:^(dispatch_semaphore_t semaphore2) {
                    NSDictionary *dictItem=[[myCart objectForKey:@"itemsList"] objectAtIndex:i];
                    NSMutableDictionary *dictItemToSend=[[NSMutableDictionary alloc] init];
                    [dictItemToSend setObject:[NSNumber numberWithInt:[[dictItem objectForKey:@"id"] intValue] ] forKey:@"product"];
                    [dictItemToSend setObject:[NSNumber numberWithInt:[[self->dictOrderCreated
                                               objectForKey:@"id"] intValue]] forKey:@"order"];
                    if ([[dictItem objectForKey:@"observations"] length]>0) {
                        [dictItemToSend setObject:[dictItem objectForKey:@"observations"] forKey:@"observations"];
                    }
                    InvokeService *invoke2=[[InvokeService alloc] init];
                    [invoke2 addItemToOrderWithData:dictItemToSend WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                        if (!error) {
                            NSLog(@"item added %d",i);
                        }
                        dispatch_semaphore_signal(semaphore2);
                    }];
                }];
            }
            dispatch_semaphore_signal(semaphore);
        }
        else{
             dispatch_semaphore_signal(semaphore);
        }
    }];
    [self performAndWait:^(dispatch_semaphore_t semaphore) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[self->dictOrderCreated allKeys] count]>3) {
                InvokeService *invoke=[[InvokeService alloc] init];
                [invoke provisionalNotifyOrderwithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    NSLog(@"sending push to shop");
                }];
                [SVProgressHUD dismiss];
                NSLog(@"finishing");
                myCart= nil;
                [self showNoItemsView];
                [[SharedFunctions sharedInstance] setBadgeCart:0];
                [[SharedFunctions sharedInstance] showAlertWithMessage:[NSString stringWithFormat:@"Se ha generado el número de orden %@. Le notificaremos cuando el establecimiento actualice su orden",[self->dictOrderCreated objectForKey:@"id"]]];
            }
            else{
                [SVProgressHUD dismiss];
                [[SharedFunctions sharedInstance] showAlertWithMessage:[NSString stringWithFormat:@"Error al generar la orden. Intente de nuevo"]];
            }
            
        });
        dispatch_semaphore_signal(semaphore);
    }];
    
}
- (void)performAndWait:(void (^)(dispatch_semaphore_t semaphore))perform;
{
    NSParameterAssert(perform);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    perform(semaphore);
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}
- (void)performAndWait2:(void (^)(dispatch_semaphore_t semaphore2))perform;
{
    NSParameterAssert(perform);
    dispatch_semaphore_t semaphore2 = dispatch_semaphore_create(0);
    perform(semaphore2);
    dispatch_semaphore_wait(semaphore2, DISPATCH_TIME_FOREVER);
}
@end
