//
//  RecoverPassVC.m
//  YaEsta
//
//  Created by Martin Gonzalez on 21/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

#import "RecoverPassVC.h"
#import "InvokeService.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "SVProgressHUD.h"

@interface RecoverPassVC ()

@end

@implementation RecoverPassVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [_txtFieldEmail becomeFirstResponder];
}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionNext:(id)sender {
    [self dismissKeyboard];
    if (_txtFieldEmail.text.length<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar un Email"];
        return;
    }
    if (![self validateEmailWithString:_txtFieldEmail.text]) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar un Email válido"];
        return;
    }
    [SVProgressHUD show];
    InvokeService *invoke=[[InvokeService alloc] init];
    NSMutableDictionary *dict=[NSMutableDictionary new];
    [dict setObject:_txtFieldEmail.text forKey:@"email"];
    
    [invoke recoverPassWithInfo:dict WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        NSLog(@"response");
        [SVProgressHUD dismiss];
            if ([[responseData allKeys] count]>0) {
                if ([[responseData objectForKey:@"email"] length]>0) {
                    UIAlertController * alert=  [UIAlertController
                                                 alertControllerWithTitle:@"YaEstá!"
                                                 message:@"Revisa tu correo electrónico, te hemos enviado las instrucciones para restaurar tu contraseña."
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    
                    UIAlertAction* okAction = [UIAlertAction
                                               actionWithTitle:@"Entendido"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   [self dismissViewControllerAnimated:YES completion:nil];
                                                   
                                               }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else{
                [[SharedFunctions sharedInstance] showAlertWithMessage:@"El email no ha sido registrado"];
            }
        
        
    }];
    
    
    
}

@end
