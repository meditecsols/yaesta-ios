//
//  ProfileViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 02/07/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit
import SDWebImage


class ProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var img_options_profile: UIImageView!
    
    @IBOutlet weak var lbl_options_profile: UILabel!
    
}

class ProfileViewController: UIViewController,  UITableViewDataSource, UITableViewDelegate {
    var profile_options: [String] = []

    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var img_profile: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let account_dictionary = accountInfo as? [String:AnyObject]
        let profile_dictionary = userRetrievedInfo as? [String:AnyObject]
        
        
        lbl_email.text = account_dictionary?["email"] as? String
        
        if profile_dictionary?["first_name"] != nil
        {
             lbl_name.text = profile_dictionary?["first_name"] as? String
        }
        else
        {
        lbl_name.text = "Completa tu perfil"
        }
        
        lblVersion.text = "Versión: " + (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String)!
        //profile_options = ["Pago", "Historial", "Promociones", "Ayuda", "Editar perfil", "Contraseña"]
        profile_options = ["Pago","Historial", "Ayuda", "Editar perfil", "Contraseña"]
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        let invoke = InvokeService()
        invoke .getCustomerImage { (response,error) in
            if urlImageUser.count > 0{
                self.img_profile.sd_setImage(with: URL(string: urlImageUser), placeholderImage: UIImage(named: "unknowngray.png"))
                
                self.roundImage()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profile_options.count
    }
    func roundImage() {
        self.img_profile.layer.borderWidth = 1
        self.img_profile.layer.masksToBounds = false
        self.img_profile.layer.borderColor = UIColor.blue.cgColor
        self.img_profile.layer.cornerRadius = self.img_profile.frame.height/2
        self.img_profile.clipsToBounds = true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profile_cell", for: indexPath) as! ProfileTableViewCell
        
        var imageName = ""
        
        switch indexPath.row {
        case 0:
            imageName = "pagos"
            break
        case 1:
            imageName = "historial"
            break
        case 2:
              imageName = "ayuda"
            break
        case 3:
             imageName = "editProfile"
            break
            
        case 4:
            
            imageName = "password"
            break

            
        default:
             imageName = "pagos"
        }
        
        let image_options = UIImage(named: imageName)
        cell.img_options_profile.image = image_options
        
        cell.lbl_options_profile.text = profile_options[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: "payment_segue", sender: self)
            break
        case 1:
            self.performSegue(withIdentifier: "record_segue", sender: self)
           
            break
        case 2:
            //self.performSegue(withIdentifier: "record_segue", sender: self)
            self.performSegue(withIdentifier: "help_segue", sender: self)
            break
        case 3:
               self.performSegue(withIdentifier: "edit_profile_segue", sender: self)
            break
        case 4:
            self.performSegue(withIdentifier: "change_pass_segue", sender: self)
            break
  
        default:
            self.performSegue(withIdentifier: "payment_segue", sender: self)
            break
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "payment_segue" {
            
        }
        else if segue.identifier == "register_segue"
        {
            
        }
        else if segue.identifier == "list_shops_segue"
        {
            
        }
        else if segue.identifier == "payment_segue"
        {
            
        }
        else if segue.identifier == "help_segue"
        {
            
        }
        else if segue.identifier == "edit_profile_segue"
        {
            
        }
        else if segue.identifier == "change_pass_segue"
        {
            
        }
    }
 

}
