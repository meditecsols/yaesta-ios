//
//  ShopViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 27/06/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit
import SVProgressHUD


class RecommendedTableViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_type: UIButton!
    
}

class ProductsDetailsTableViewCell: UITableViewCell{
    
    @IBOutlet weak var lbl_product: UILabel!
    
    @IBOutlet weak var lbl_product_details: UILabel!
    
    @IBOutlet weak var lbl_price: UILabel!
}

class ShopViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource{
  
   
    
    var recommended: [String] = []
    var products : NSMutableArray = []
    var indexbutton = 0
    @IBOutlet weak var recommendedCollection: UICollectionView!
    @IBOutlet weak var lbl_title_recommended: UILabel!
    @IBOutlet weak var tv_products: UITableView!
    
    @IBOutlet weak var img_shop: UIImageView!
    @IBOutlet weak var lbl_description_shop: UILabel!
    @IBOutlet weak var lbl_name_shop: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.show()
        
        let shop  = selectedShop as! [String : AnyObject]
        let shop_id = shop["id"] as! Int
        self.lbl_name_shop.text = shop["name"] as? String
        self.lbl_description_shop.text = shop["description"] as? String
        
        let shop_images = (selectedShop["picture_set"]! as! NSArray).mutableCopy() as! NSMutableArray
        if shop_images.count > 0
        {
        let shop_image_dic = shop_images[0] as! [String : AnyObject]
        let shop_image_url = shop_image_dic["picture"] as? String
        let imageUrl:URL = URL(string: shop_image_url!)!
        
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let imageData:NSData = NSData(contentsOf: imageUrl)!

            // When from background thread, UI needs to be updated on main_queue
            DispatchQueue.main.async {
                let image = UIImage(data: imageData as Data)
                self.img_shop.image = image
                //self.img_shop.contentMode = UIViewContentMode.scaleAspectFit
              
            }
        }
        }
        
        let invoke = InvokeService()
        
        invoke.getShopProducts(String(shop_id)) { (response, error) in
            
            self.products = response!
            SVProgressHUD.dismiss()
         
            DispatchQueue.main.async {
               self.tv_products.reloadData()
            }
        }
        
        
        recommended = ["Recomendado", "Lo caliente", "Lo frío", "Sandwiches", "Paninis"]
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recommended.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recommended_cell", for: indexPath) as! RecommendedTableViewCell
        
        let recommendedName = recommended[indexPath.row]
        cell.lbl_type?.setTitle(recommendedName, for: .normal)
        cell.lbl_type.tag = indexPath.row
        cell.lbl_type.addTarget(self, action: #selector(editButtonTapped), for: UIControlEvents.touchUpInside)
        cell.lbl_type.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.0)
        cell.lbl_type.setTitleColor(UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1), for: .normal)
            
        return cell
    }


    
    @objc func editButtonTapped(button: UIButton) {
        print(button.tag)
        
        lbl_title_recommended.text = button.titleLabel?.text
        
        for (index, element) in recommended.enumerated() {
            print("Item \(index): \(element)")
            if index == button.tag
            {
                button.backgroundColor = UIColor(red: 50/255, green: 96/255, blue: 255/255, alpha: 1)
                button.setTitleColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1), for: .normal)
            }
            else
            {
                let indexP = IndexPath(row: index, section: 0)
                recommendedCollection.reloadItems(at: [indexP])
               
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "products_cell", for: indexPath) as! ProductsDetailsTableViewCell
        let productsDict  = products[indexPath.row] as! [String : AnyObject]
        
        cell.lbl_product.text = productsDict["name"] as? String
        cell.lbl_product_details.text = productsDict["description"] as? String
        cell.lbl_price.text = "$" + (productsDict["price"] as? String)!
      
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 103
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "product_detail_segue", sender: self)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
       
        if segue.identifier == "product_detail_segue" {
            if let indexPath = self.tv_products.indexPathForSelectedRow{
                let selectedRow = indexPath.row
                let productsDict  = products[selectedRow] as! [String : AnyObject]
                tv_products.deselectRow(at: indexPath, animated: true)
                let Pr : ProductsDetailsViewController = segue.destination as! ProductsDetailsViewController
                Pr.item = productsDict as NSDictionary
                Pr.shop = selectedShop
                
            }
           
        }
        else if segue.identifier == "information_segue"
        {
           
        }
    }

 
    @IBAction func action_informacion(_ sender: Any) {
        self.performSegue(withIdentifier: "information_segue", sender: self)
    }
    
    @IBAction func back_action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    
    }
}
