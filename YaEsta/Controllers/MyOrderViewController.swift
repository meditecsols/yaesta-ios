//
//  MyOrderViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 12/07/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit

class MyOrderTableViewCell: UITableViewCell {
  
    @IBOutlet weak var lbl_product: UILabel!
    @IBOutlet weak var lbl_cost_product: UILabel!
    
    
}

class MyOrderViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPopoverPresentationControllerDelegate {
    
    var myorder_list: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myorder_list    = ["Espresso", "Sandwich de queso y pavo", "Panqué de elote"]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myorder_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "myorder_cell", for: indexPath) as! MyOrderTableViewCell
        
        cell.lbl_product.text = myorder_list[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func order_action(_ sender: Any) {
        let VC = storyboard?.instantiateViewController(withIdentifier: "popup") as! PopOverOrderResultViewController
        VC.preferredContentSize = CGSize(width: 335, height: 403)
        VC.modalPresentationStyle = UIModalPresentationStyle.popover
        
        
        
        
        
    }
    
}
