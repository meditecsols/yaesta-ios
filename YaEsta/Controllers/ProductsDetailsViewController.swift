//
//  ProductsDetailsViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 27/06/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit
import GMStepper

class ProductsDetailsViewController: UIViewController {

    var item = NSDictionary()
    var shop = NSDictionary()
    
    @IBOutlet weak var lbl_product_price: UILabel!
    @IBOutlet weak var lbl_product_detail: UILabel!
    @IBOutlet weak var lbl_product_name: UILabel!
    @IBOutlet weak var img_product: UIImageView!
    @IBOutlet weak var stepper: GMStepper!
    
    @IBOutlet weak var txt_product_comments: CustomField!
    
    @IBOutlet weak var btn_order: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:)))
        
        self.view.addGestureRecognizer(tap)

        self.lbl_product_name.text = item["name"] as? String
        self.lbl_product_detail.text = item["description"] as? String
        self.lbl_product_price.text = "$" + (item["price"] as? String)!
        self.btn_order.setTitle("Agregar a la orden       " + self.lbl_product_price.text! , for: .normal)
        let product_images = (item["picture_set"]! as! NSArray).mutableCopy() as! NSMutableArray
        
        if product_images.count > 0
        {
        let product_image_dic = product_images[0] as! [String : AnyObject]
        let product_image_url = product_image_dic["picture"] as? String
        let imageUrl:URL = URL(string: product_image_url!)!
        
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let imageData:NSData = NSData(contentsOf: imageUrl)!
            
            
            // When from background thread, UI needs to be updated on main_queue
            DispatchQueue.main.async {
                let image = UIImage(data: imageData as Data)
                self.img_product.image = image
                //self.img_product.contentMode = UIViewContentMode.scaleAspectFill
                
            }
        }
        }
        // Do any additional setup after loading the view.
    }
    @objc func handleTap(gesture: UITapGestureRecognizer) {
        self.view .endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func back_action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
        
    }
    @IBAction func change_price_action(_ sender: UIStepper) {
        var value = sender.value
        let price = item["price"] as? String
        value = Double(price!)! * value
       
        self.btn_order.setTitle("Agregar a la orden       $" + String(value) , for: .normal)
        SharedFunctions.sharedInstance().setBadgeCart(2)
        
    }
    @IBAction func actionAddToCart(_ sender: Any) {
        let value = Int(stepper.value)
        for _ in 0...value-1{
            let itemNew = NSMutableDictionary(dictionary: item as! [AnyHashable : Any], copyItems: true)
            if (txt_product_comments.text?.count)!>0{
                itemNew["observations"] = txt_product_comments.text
            }
            
            SharedFunctions.sharedInstance().addItem(toCart: NSMutableDictionary(dictionary: itemNew), ofShop: NSMutableDictionary(dictionary: shop))
        }
        
        
        txt_product_comments.text = "";
        
        let alertController = UIAlertController(title: "YaEstá!", message: "Se ha agregado exitosamente el producto", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Ir a Mi Orden", style: .default) { (action:UIAlertAction) in
            SharedFunctions.sharedInstance().goToCart()
        }
        
        let action2 = UIAlertAction(title: "Seguir comprando", style: .cancel) { (action:UIAlertAction) in
            alertController.dismiss(animated: true, completion: {
                NSLog("stay")
            });
        }

        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
        
    }
}
