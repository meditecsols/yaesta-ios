//
//  RegisterViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 22/06/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit
import FacebookCore
import FBSDKLoginKit
import GoogleSignIn


class RegisterViewController: UIViewController , GIDSignInUIDelegate,GIDSignInDelegate{

    @IBOutlet weak var lbl_terms_policy: UILabel!
    
    @IBOutlet weak var txt_name: UITextField!
    
    @IBOutlet weak var txt_email: UITextField!
    
    @IBOutlet weak var txt_password: UITextField!
    
    @IBOutlet weak var txt_confirmpassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().delegate = self
        
        let tapterm = UITapGestureRecognizer(target: self, action: #selector(tapTerms(gesture:)))
        lbl_terms_policy.addGestureRecognizer(tapterm)
      
        // Do any additional setup after loading the view.
    }

    @objc func tapTerms(gesture: UITapGestureRecognizer) {
        let text = (lbl_terms_policy.text)!
        let termsRange = (text as NSString).range(of: "Términos y Condiciones")
        let privacyRange = (text as NSString).range(of: "Aviso de Privacidad")
        
        if gesture.didTapAttributedTextInLabel(label: lbl_terms_policy, inRange: termsRange) {
            print("Tapped terms")
             self.performSegue(withIdentifier: "terms_segue", sender: self)
        } else if gesture.didTapAttributedTextInLabel(label: lbl_terms_policy, inRange: privacyRange) {
            self.performSegue(withIdentifier: "privacy_segue", sender: self)
        } else {
            print("Tapped none")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func back_action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }

    
    @IBAction func action_registro(_ sender: Any) {
    }
    @IBAction func action_facebook(_ sender: Any) {
     
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
        
        
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    //print(result)
                    let data:[String:AnyObject] = result as! [String : AnyObject]
                    print(data["first_name"]!)
                
                }
            })
        }
    }
    
    @IBAction func action_google(_ sender: Any) {
           GIDSignIn.sharedInstance().signIn()
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "terms_segue" {
            
        }
        else if segue.identifier == "privacy_segue"
        {
            
        }
    }
 
    
    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    private func signInWillDispatch(signIn: GIDSignIn!, error: Error!) {
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        	
        self.dismiss(animated: true, completion: nil)
    }
    
        func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                  withError error: Error!) {
            if let error = error {
                print("\(error.localizedDescription)")
            } else {
                // Perform any operations on signed in user here.
                _ = user.userID                  // For client-side use only!
                _ = user.authentication.idToken // Safe to send to the server
                _ = user.profile.name
                _ = user.profile.givenName
                _ = user.profile.familyName
                _ = user.profile.email
                // ...
            }
        }
    
        func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
                  withError error: Error!) {
            // Perform any operations when the user disconnects from app here.
            // ...
        }

    
}
