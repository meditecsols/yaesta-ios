//
//  ListShopsViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 26/06/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit

class ShopsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var img_shop: UIImageView!
    @IBOutlet weak var lbl_name_shop: UILabel!
    @IBOutlet weak var lbl_type_shop: UILabel!
    @IBOutlet weak var view_shop: UIView!
    
}

class ListShopsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    

    @IBOutlet weak var tv_shops: UITableView!
    
    var shops: [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        shops = ["Capeltic", "Society6", "La Terraza", "El Cubo", "El Trebol"]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shopcell", for: indexPath) as! ShopsTableViewCell
        
        let shopName = shops[indexPath.row]
        cell.lbl_name_shop?.text = shopName
        cell.lbl_type_shop?.text = "Cafetería"
        cell.img_shop?.image = UIImage(named: "coffee.png")
        cell.view_shop.layer.cornerRadius=5
        cell.view_shop.layer.masksToBounds=true
        
        
        // set the shadow properties
        cell.view_shop.layer.shadowColor = UIColor.black.cgColor
        cell.view_shop.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.view_shop.layer.shadowOpacity = 0.2
        cell.view_shop.layer.shadowRadius = 4.0
        //cell.view_shop.layer.shadowPath = UIBezierPath(rect: cell.view_shop.bounds).cgPath
        //cell.view_shop.layer.shouldRasterize = true
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shops.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "shop_segue", sender: self)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "shop_segue" {
            
        }
    }
 

}
