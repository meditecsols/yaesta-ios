//
//  CompleteProfileViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 30/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit
import SVProgressHUD
class CompleteProfileViewController: UIViewController {
    
    @IBOutlet weak var txt_name: UITextField!
    
    @IBOutlet weak var txt_last_name: UITextField!
    
    @IBOutlet weak var txt_second_name: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:)))
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    @objc func handleTap(gesture: UITapGestureRecognizer) {
        self.view .endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func save_action(_ sender: Any) {
        
        self.view .endEditing(true)
        if txt_name.text?.isEmpty ?? true {
            SharedFunctions .sharedInstance().showAlert(withMessage: "Ingresa un nombre.")
            return
        }
        if txt_last_name.text?.isEmpty ?? true {
            SharedFunctions .sharedInstance().showAlert(withMessage: "Ingresa un apellido paterno.")
            return
        }
        if txt_second_name.text?.isEmpty ?? true {
            SharedFunctions .sharedInstance().showAlert(withMessage: "Ingresa un apellido materno.")
            return
        }
        
        
        let invoke  = InvokeService()
        
        let dict = NSMutableDictionary()
        
        dict["usr"]=accountInfo!["pk"] as! Int
        dict["name"]=txt_name.text
        dict["last"]=txt_last_name.text
        dict["second"]=txt_second_name.text
        

        invoke.registerCustomer(dict, withCompletion: { (response, error) in
            if error != nil {
                SharedFunctions .sharedInstance().showAlert(withMessage: "Error al completar tu perfil.")
                return
            }
            if (response != nil)
            {
                if (response?.count)! > 0
                {
                
                    userRetrievedInfo=NSMutableDictionary(dictionary: SharedFunctions.sharedInstance().dictionaryByReplacingNulls(withStrings:response ))
                    SharedFunctions.sharedInstance().saveUserData(userRetrievedInfo! as! [AnyHashable : Any])
                    SharedFunctions.sharedInstance().saveUser(accountInfo!["usr"] as! String, andPass: accountInfo!["psw"] as! String)
                    self.performSegue(withIdentifier: "list_shops_segue", sender: self)
                }
            }
            
           /* let invoke2 = InvokeService()
            invoke2.getCustomerWithCompletion({ (response, error) in
                
                if error != nil {
                    SharedFunctions .sharedInstance().showAlert(withMessage: "Error al Iniciar Sesión.")
                    return
                }
                
                if (response != nil)
                {
                    let profile = response![0] as! NSDictionary
                    let profileMutableDict: NSMutableDictionary = NSMutableDictionary(dictionary: profile)
                    userRetrievedInfo=NSMutableDictionary(dictionary: SharedFunctions.sharedInstance().dictionaryByReplacingNulls(withStrings:profileMutableDict ))
                     self.performSegue(withIdentifier: "list_shops_segue", sender: self)
                    
                }
                
                
            })*/
            
            //SharedFunctions.sharedInstance().updatePushToken(fcmTokenRetrieved as String!)
        })
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
