//
//  QuestionsHelpViewController.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 10/07/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit

class QuestionsHelpTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_question: UILabel!
    
    
}

class QuestionsHelpViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    

    var question_list: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

         question_list = ["Mi pedido no ha sido confirmado", "Cancelaciones", "¿Cómo cambio mi pedido?","Hubo un error en mi pedido"]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return question_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "question_cell", for: indexPath) as! QuestionsHelpTableViewCell
        
        cell.lbl_question.text = question_list[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    @IBAction func back_action(_ sender: Any) {
        
    navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "details_question_segue", sender: self)
    }
   
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "details_question_segue" {
            
        }
       
     }
 
    
}
