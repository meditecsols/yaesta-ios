//
//  InvokeService.h

//
//  Created by Martin Gonzalez on 27/09/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface InvokeService : NSObject
- (void)loginWithCompletion:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)tokenWithInfo:(NSMutableDictionary *_Nullable)dictInfo WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)registerWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)retrieveZipCodeData:(NSString *_Nullable)zipCode WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getCustomerWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)updateCustomerWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getCustomerImageWithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)updateCustomerImageWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getUserData:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)registerCustomer:(NSMutableDictionary *_Nullable)dict WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addAddressWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getAddressOfUser:(NSString *_Nullable)idUser     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)recoverPassWithInfo:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getUrlsTermsPrivacyWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
-(NSString *)parseToTinyURL:(NSString *)urlToShare;
- (void)getShopsByUserWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
-(void)getShopProducts:(NSString *)id_shop withCompletion:(void(^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addCardWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getCardsWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getCardDefaultWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)updateCardWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)deleteCard:(NSString *_Nullable)idCard     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)sendPaymentConektaWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addOrderWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addItemToOrderWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)provisionalNotifyOrderwithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getOrderWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
@end
