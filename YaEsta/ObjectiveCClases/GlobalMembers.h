//
//  GlobalMembers.h
//
//  Created by Martin Gonzalez on 15/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalMembers : NSObject
extern NSString *fullNameUser;
extern NSString *usr;
extern NSString *psw;
extern NSString *urlImageUser;
extern NSMutableDictionary *accountInfo;
extern BOOL firstTimeEntry;
extern BOOL comeFromLogin;
extern NSMutableDictionary *userRetrievedInfo;
extern NSMutableDictionary *myCart;
extern NSMutableString *fcmTokenRetrieved;
extern NSMutableDictionary *selectedShop;
@end
