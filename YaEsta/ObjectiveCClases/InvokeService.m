//
//  InvokeService.m

//
//  Created by Martin Gonzalez on 27/09/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "InvokeService.h"
#import "Config.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"
@import MobileCoreServices;

@implementation InvokeService

- (void)loginWithCompletion:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSDictionary *headers = @{ @"content-type": @"application/json",};
    NSDictionary *parameters = @{ @"username": [dictInfo objectForKey:@"usr"],
                                  @"password": [dictInfo objectForKey:@"psw"] };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/rest-auth/login/",BASE_URL]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
- (void)tokenWithInfo:(NSMutableDictionary *)dictInfo WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[self encodeStringTo64:[NSString stringWithFormat:@"%@:%@",[dictInfo objectForKey:@"client"] ,[dictInfo objectForKey:@"secret"]]];
    authString=[NSString stringWithFormat:@"Basic %@",authString];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization": authString, };
    NSDictionary *parameters = @{ @"grant_type": @"password",
                                  @"username": [dictInfo objectForKey:@"usr"],
                                  @"password": [dictInfo objectForKey:@"psw"] };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/o/token/",BASE_URL]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}


#pragma mark Register
- (void)registerWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    //NSString *authString=[self encodeStringTo64:[NSString stringWithFormat:@"%@:%@",client,secret]];
   // authString=[NSString stringWithFormat:@"Basic %@",authString];
    NSDictionary *headers = @{ @"content-type": @"application/json"};
    
    NSDictionary *parameters = @{
                                
                                 @"password1":[dictInfo objectForKey:@"pass"],
                                 @"password2":[dictInfo objectForKey:@"pass"],
                                 @"username":[dictInfo objectForKey:@"email"],
                                 @"email":[dictInfo objectForKey:@"email"],
                              
                                 };
    
    
    
    
    
    
    
    {
        
    }
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/rest-auth/registration/",BASE_URL]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark registerCustomer
- (void)registerCustomer:(NSMutableDictionary *_Nullable)dict WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion
{
    //NSString *authString=[self encodeStringTo64:[NSString stringWithFormat:@"%@:%@",client,secret]];
    // authString=[NSString stringWithFormat:@"Basic %@",authString];
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSDictionary *parameters = @{
                                 
                                 @"user":[dict objectForKey:@"usr"],
                                 @"first_name":[dict objectForKey:@"name"],
                                 @"last_name":[dict objectForKey:@"last"],
                                 @"second_last_name":[dict objectForKey:@"second"],
                                 
                                 };
    
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/customer/",BASE_URL]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark recoverPass
- (void)recoverPassWithInfo:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSDictionary *headers = @{ @"content-type": @"application/json",};
    NSDictionary *parameters = @{ @"email": [dictInfo objectForKey:@"email"] };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/login/recover/viaemail/",BASE_URL]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark GetUser
- (void)getUserData:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"] ];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/rest-auth/user/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        
                                                        
                                                       // NSString *correctString = [NSString stringWithCString:[iso cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
                                                        
                                                        //NSData *dutf8 = [correctString dataUsingEncoding:NSUTF8StringEncoding];
                                                        
                                                        
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        ws_responce=[ws_responce mutableCopy];
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getShops
- (void)getShopsByUserWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/business/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark Get Products Shop
-(void)getShopProducts:(NSString *)id_shop withCompletion:(void(^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion
{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/product/?business=%@",BASE_URL,id_shop];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
    
    
}
#pragma mark getCustomer
- (void)getCustomerWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/customer/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        if ([ws_responce isKindOfClass:[NSDictionary class]]) {
                                                            NSMutableArray *response=[[NSMutableArray alloc] init];
                                                            [response addObject:ws_responce];
                                                            completion(response,error);
                                                            return;
                                                        }
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
    
    
}
#pragma mark UpdateCustomer
- (void)updateCustomerWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization":  authString};
    
    
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    if ([[dictInfo objectForKey:@"first_name"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"first_name"] forKey:@"first_name"];
    }
    
    if ([[dictInfo objectForKey:@"clabe"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"clabe"] forKey:@"clabe"];
    }
    if ([[dictInfo objectForKey:@"date_of_birth"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"date_of_birth"] forKey:@"date_of_birth"];
    }
    
    if ([[dictInfo objectForKey:@"mobile_phone_number"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"mobile_phone_number"] forKey:@"mobile_phone_number"];
    }
    if ([[dictInfo objectForKey:@"work_phone_number"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"work_phone_number"] forKey:@"work_phone_number"];
    }
    if ([[dictInfo objectForKey:@"email"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"email"] forKey:@"email"];
    }
    if ([[dictInfo allKeys]containsObject:@"public_profile_preference"]) {
        [params setObject:[dictInfo objectForKey:@"public_profile_preference"] forKey:@"public_profile_preference"];
    }
    if ([[dictInfo allKeys]containsObject:@"first_name"]) {
        [params setObject:[dictInfo objectForKey:@"first_name"] forKey:@"first_name"];
    }
    
    
    if ([[dictInfo objectForKey:@"push_token"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"push_token"] forKey:@"token"];
    }
    else if ([fcmTokenRetrieved length]>0) {
        [params setObject:fcmTokenRetrieved forKey:@"token"];
    }
    
    NSMutableArray *arrayPaths=[[NSMutableArray alloc] init];
    NSMutableArray *arrayParameterImages=[[NSMutableArray alloc] init];
    if ([[dictInfo objectForKey:@"pathSignatureImage"] length]>0) {
        [arrayPaths addObject:[dictInfo objectForKey:@"pathSignatureImage"]];
        [arrayParameterImages addObject:@"signature"];
    }
    if ([[dictInfo objectForKey:@"pathAccountImage"] length]>0) {
        [arrayPaths addObject:[dictInfo objectForKey:@"pathAccountImage"]];
        [arrayParameterImages addObject:@"income_document"];
    }
    if ([[dictInfo objectForKey:@"pathProfileImage"] length]>0) {
        [arrayPaths addObject:[dictInfo objectForKey:@"pathProfileImage"]];
        [arrayParameterImages addObject:@"profile_pic"];
    }
    
    
    NSString *boundary = [self generateBoundaryString];

    
    //NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/customer/%@/",BASE_URL,[userRetrievedInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"PATCH"];
    [request setAllHTTPHeaderFields:headers];
   // [request setHTTPBody:postData];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:params paths:arrayPaths fieldNames:arrayParameterImages];
    
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        if ([[ws_responce allKeys] count]>=5) {
                                                            userRetrievedInfo=[[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:[ws_responce copy]] mutableCopy];
                                                        }
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getCustomerImage
- (void)getCustomerImageWithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/customerpicture/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        id ws_responceCasted=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        if ([ws_responceCasted isKindOfClass:[NSArray class]]) {
                                                            if ([ws_responceCasted count] > 0) {
                                                                if ([[[ws_responceCasted lastObject] allKeys] count]>=3) {
                                                                    if ([[[ws_responceCasted lastObject] objectForKey:@"picture"] length]>0) {
                                                                        urlImageUser= [[ws_responceCasted lastObject] objectForKey:@"picture"];
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else if([ws_responceCasted isKindOfClass:[NSDictionary class]]){
                                                            if ([[ws_responceCasted allKeys] count]>=3) {
                                                                if ([[ws_responceCasted objectForKey:@"picture"] length]>0) {
                                                                    urlImageUser= [ws_responceCasted objectForKey:@"picture"];
                                                                }
                                                            }
                                                        }
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
    
    
}
#pragma mark UpdateCustomerImage
- (void)updateCustomerImageWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization":  authString};
    
    
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

    if ([[dictInfo allKeys]containsObject:@"profile"]) {
        [params setObject:[dictInfo objectForKey:@"profile"] forKey:@"profile"];
    }
    
    NSMutableArray *arrayPaths=[[NSMutableArray alloc] init];
    NSMutableArray *arrayParameterImages=[[NSMutableArray alloc] init];

    if ([[dictInfo objectForKey:@"pathProfileImage"] length]>0) {
        [arrayPaths addObject:[dictInfo objectForKey:@"pathProfileImage"]];
        [arrayParameterImages addObject:@"picture"];
    }
    
    
    NSString *boundary = [self generateBoundaryString];
    
    
    //NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/customerpicture/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    // [request setHTTPBody:postData];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:params paths:arrayPaths fieldNames:arrayParameterImages];
    
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
        if (error) {
            NSLog(@"%@", error);
        } else {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"%@", httpResponse);
            
            
            NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
            //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
            ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            if ([[ws_responce allKeys] count]>=3) {
                if ([[ws_responce objectForKey:@"picture"] length]>0) {
                    urlImageUser= [ws_responce objectForKey:@"picture"];
                }
            }
            
        }
        completion(ws_responce,error);
    }];
    [dataTask resume];
}
#pragma mark addAddress
- (void)addAddressWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    

    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctoraddress/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark updateAddress
- (void)updateAddressWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctoraddress/%@/",BASE_URL,[dictInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"PATCH"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        int code=(int)[httpResponse statusCode];
                                                        if (code==200) {
                                                           // NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                            //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                            ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        }
                                                        
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getAddress
- (void)getAddressOfUser:(NSString *_Nullable)idUser     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
 
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctoraddress/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getUrls
- (void)getUrlsTermsPrivacyWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/sysurl/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addCard
- (void)addCardWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/conektacard/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getCards
- (void)getCardsWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/conektacard/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getCardDefaulr
- (void)getCardDefaultWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/card/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark updateCard
- (void)updateCardWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/conektacard/%@/",BASE_URL,[dictInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [dictInfo removeObjectForKey:@"id"];
    [request setHTTPMethod:@"PATCH"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark deletetCard
- (void)deleteCard:(NSString *_Nullable)idCard     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/conektacard/%@/",BASE_URL,idCard];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"DELETE"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSError *errorParsing;
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&errorParsing];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark sendPaymentConekta
- (void)sendPaymentConektaWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/conektacharge/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addOrder
- (void)addOrderWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/order/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addItemOrder
- (void)addItemToOrderWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/item/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark -
#pragma mark utilMethods
#pragma mark RetrieveZIPCodeData
- (void)retrieveZipCodeData:(NSString *)zipCode WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@",URL_ZIP_CODE,zipCode];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        
                                                        
                                                        NSString *correctString = [NSString stringWithCString:[iso cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
                                                        
                                                        NSData *dutf8 = [correctString dataUsingEncoding:NSUTF8StringEncoding];
                                                        
                                                        
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:dutf8 options:NSJSONReadingAllowFragments error:&error];
                                                        ws_responce=[ws_responce mutableCopy];
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
- (void)provisionalNotifyOrderwithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    
    NSString *urlString=[NSString stringWithFormat:@"http://pamliljehult.inntecmovil.com/notification.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        
                                                        
                                                        NSString *correctString = [NSString stringWithCString:[iso cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
                                                        
                                                        NSData *dutf8 = [correctString dataUsingEncoding:NSUTF8StringEncoding];
                                                        
                                                        
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:dutf8 options:NSJSONReadingAllowFragments error:&error];
                                                        ws_responce=[ws_responce mutableCopy];
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark Get Orders Records

#pragma mark getCards
- (void)getOrderWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Token %@",[accountInfo objectForKey:@"key"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/orderinfo/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}

#pragma mark -
- (NSString *)mimeTypeForPath:(NSString *)path {
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}
- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}
- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                             paths:(NSArray *)paths
                         fieldNames:(NSArray *)fieldNames {
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    int i = 0;
    for (NSString *path in paths) {
        NSString *filename  = [path lastPathComponent];
        NSData   *data      = [NSData dataWithContentsOfFile:path];
        NSString *mimetype  = [self mimeTypeForPath:path];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", [fieldNames objectAtIndex:i], filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        i++;
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}
- (NSString*)encodeStringTo64:(NSString*)fromString
{
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    }
    return base64String;
}
//- (void)elWithInfo:(NSArray *)info {
//    NSString *POST_BODY_BOURDARY=@"asdfsdf";
//    NSURL *url = [NSURL URLWithString:@"http://127.0.0.1/uploadimage.php"];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:5.0];
//    [request setHTTPMethod:@"POST"];
//    NSString *contentTypeValue = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", POST_BODY_BOURDARY];
//    [request addValue:contentTypeValue forHTTPHeaderField:@"Content-type"];
//    NSMutableData *dataForm = [NSMutableData alloc];
//    [dataForm appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",POST_BODY_BOURDARY] dataUsingEncoding:NSUTF8StringEncoding]];
//    [dataForm appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"param1\";\r\n\r\n%@", @"10001"] dataUsingEncoding:NSUTF8StringEncoding]];
//    if([info count] > 0) {
//        int i = 0;
//        for (NSDictionary *imageInfo in info) {
//            ///UIImage *image = [imageInfo valueForKey:UIImagePickerControllerOriginalImage];
//            //image = [self resizeImage:image minSize:700.0];
//            //NSData *imageData = UIImageJPEGRepresentation(image, 0.9);
//            [dataForm appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",POST_BODY_BOURDARY] dataUsingEncoding:NSUTF8StringEncoding]];
//            [dataForm appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fileToUpload[]\"; filename=\"%@%d.jpg\"\r\n", @"test", i] dataUsingEncoding:NSUTF8StringEncoding]];
//            [dataForm appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//            [dataForm appendData:[NSData dataWithData:imageData]];
//            i++;
//        }
//    }
//    [dataForm appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",POST_BODY_BOURDARY] dataUsingEncoding:NSUTF8StringEncoding]];
//    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:dataForm];
//    [uploadTask resume];
//}
-(NSString *)parseToTinyURL:(NSString *)urlToShare{
    
    NSString *urlData =[NSString stringWithFormat:@"https://tinyurl.com/api-create.php?url=%@",urlToShare];
    
    NSError *error;
    
    NSString *shortURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlData]
                                                  encoding:NSASCIIStringEncoding
                                                     error:&error];
    return shortURL;
}
@end
