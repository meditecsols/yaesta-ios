//
//  SharedFunctions.m

//
//  Created by Martin Gonzalez on 03/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "InvokeService.h"
#import "Config.h"

@interface SharedFunctions()
@end
@implementation SharedFunctions

+(SharedFunctions *)sharedInstance{
    
    static SharedFunctions *_sharedInstance =  nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[SharedFunctions alloc] init];
    });
    
    return _sharedInstance;
}

-(id)init{
    self = [super init];
    if(self){
        
    }
    return self;
}

-(UIViewController*)topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"YaEstá!"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [[self topMostController] presentViewController:alert animated:NO completion:nil];
}



-(void)saveUser:(NSString *)usr andPass:(NSString *)pass{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:usr forKey:@"usr"];
    [def setObject:pass forKey:@"pass"];
    usr=usr;
    psw=pass;
    [def synchronize];
}
-(void)getUsrAndPass{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    usr = [def valueForKey:@"usr"];
    psw =[def valueForKey:@"pass"];
}
-(void)saveUserData:(NSDictionary *)usrInfo{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:[self dictionaryByReplacingNullsWithStrings:usrInfo] forKey:@"usrInfo"];
    [def setObject:[self dictionaryByReplacingNullsWithStrings:accountInfo] forKey:@"accessToken"];
    [def synchronize];
}
-(void)getUsrInfo{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    userRetrievedInfo=[def objectForKey:@"usrInfo"];
    accountInfo=[def objectForKey:@"accessToken"];
    
}
-(void)clearUsrAndPass{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:@"" forKey:@"usr"];
    [def setObject:@"" forKey:@"pass"];
    [def setObject:nil forKey:@"usrInfo"];
    usr=@"";
    psw=@"";
    userRetrievedInfo=nil;
    accountInfo=nil;
    urlImageUser=nil;
    myCart=nil;
    [def synchronize];
}
-(BOOL) isiPad{
    static BOOL isIPad = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isIPad = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad;
    });
    return isIPad;
}
-(NSString *)getCurrentStoryboard{
    NSString *nameStoryboard;
    if ([self isiPad]) {
        nameStoryboard =@"iPadStoryboard";
    }
    else{
        nameStoryboard = @"Main";
    }
    
    return nameStoryboard;
}
-(void)updatePushToken:(NSString *)token{
    if (token.length<=0) {
        return;
    }
    if ([userRetrievedInfo objectForKey:@"id"]) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        [dict setObject:token forKey:@"push_token"];
        [dict setObject:[userRetrievedInfo objectForKey:@"id"] forKey:@"id"];
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke updateCustomerWithData:dict WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            NSLog(@"updating fcmToken: %@",responseData);
            
        }]; 
    }
    
    
}
-(NSString *)formatDateToWS:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}
-(NSMutableDictionary *) dictionaryByReplacingNullsWithStrings : (NSMutableDictionary *) dictionary{
    const NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary: dictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for (NSString *key in dictionary) {
        const id object = [dictionary objectForKey: key];
        if (object == nul) {
            [replaced setObject: blank forKey: key];
        }
        else if ([object isKindOfClass: [NSDictionary class]]) {
            [replaced setObject: [self dictionaryByReplacingNullsWithStrings:object] forKey: key];
        }else if ([object isKindOfClass: [NSArray class]]){
            [replaced setObject:[self arrayByReplacingNullsWithBlanks:object] forKey:key];
        }
    }
    return  replaced;
}

-(NSArray *)arrayByReplacingNullsWithBlanks  : (NSArray *) array{
    NSMutableArray *replaced = [array mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    for (int idx = 0; idx < [replaced count]; idx++) {
        id object = [replaced objectAtIndex:idx];
        if (object == nul) [replaced replaceObjectAtIndex:idx withObject:blank];
        else if ([object isKindOfClass:[NSDictionary class]])
            [replaced replaceObjectAtIndex:idx withObject:[self dictionaryByReplacingNullsWithStrings:object]];
        else if ([object isKindOfClass:[NSArray class]])
            [replaced replaceObjectAtIndex:idx withObject:[self arrayByReplacingNullsWithBlanks: object]];
    }
    return [replaced copy];
}


-(NSString *)getDateFormatToWS {
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter2 setLocale:enUSPOSIXLocale];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *dateFinal=[dateFormatter2 stringFromDate:[NSDate date]];
    return dateFinal;
}

-(BOOL)addItemToCart:(NSMutableDictionary *)dictItem ofShop:(NSMutableDictionary *)dictShop{
    if (!myCart) {
        myCart=[[NSMutableDictionary alloc] init];
    }
    int businessId=[[myCart objectForKey:@"business"] intValue];
    if ([[dictItem objectForKey:@"business"] intValue] != businessId && businessId >0) {
        [self showAlertWithMessage:@"No puedes combinar pedidos de diferentes tiendas."];
        return NO;
    }
    NSMutableArray *arrayItems=[[NSMutableArray alloc] initWithArray:[myCart objectForKey:@"itemsList"]];
    [arrayItems addObject:dictItem];
    [myCart setObject:[NSNumber numberWithInt:[[dictItem objectForKey:@"business"] intValue]] forKey:@"business"];
    [myCart setObject:arrayItems forKey:@"itemsList"];
    [myCart setObject:dictShop forKey:@"businessInfo"];
    [self setBadgeCart:(int)[arrayItems count]];
    
    return YES;
    
}
-(void)deleteItemToCartAtIndex:(int)index{
    NSMutableArray *arrayItems=[[NSMutableArray alloc] initWithArray:[myCart objectForKey:@"itemsList"]];
    [arrayItems removeObjectAtIndex:index];
    [myCart setObject:arrayItems forKey:@"itemsList"];
    [self setBadgeCart:(int)[arrayItems count]];
}
-(void)updateItemToCart:(NSMutableDictionary *)dictItem{
    
}
-(void)setBadgeCart:(int)number{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    if ([topController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabTopController=(UITabBarController *)topController;
        [[tabTopController.tabBar.items objectAtIndex:2] setBadgeValue:[NSString stringWithFormat:@"%d",number]];
        if (number==0) {
            [tabTopController.tabBar.items objectAtIndex:2].badgeValue=nil;
        }
    }else{
        while (topController.presentedViewController) {
            topController = topController.presentedViewController;
            if ([topController isKindOfClass:[UITabBarController class]]) {
                UITabBarController *tabTopController=(UITabBarController *)topController;
                [[tabTopController.tabBar.items objectAtIndex:2] setBadgeValue:[NSString stringWithFormat:@"%d",number]];
                if (number==0) {
                    [tabTopController.tabBar.items objectAtIndex:2].badgeValue=nil;
                }
            }
        }
    }
    
    
}
-(void)goToCart{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    if ([topController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabTopController=(UITabBarController *)topController;
        [tabTopController setSelectedIndex:2];
    }else{
        while (topController.presentedViewController) {
            topController = topController.presentedViewController;
            if ([topController isKindOfClass:[UITabBarController class]]) {
                UITabBarController *tabTopController=(UITabBarController *)topController;
                [tabTopController setSelectedIndex:2];
            }
        }
    }
    if (![[self topMostController] isKindOfClass:[UITabBarController class]]) {
        [[self topMostController] dismissViewControllerAnimated:NO completion:^{
            [self goToCart];
        }];
    }
    
}

@end
