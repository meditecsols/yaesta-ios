//
//  SharedFunctions.h

//
//  Created by Martin Gonzalez on 03/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>



@interface SharedFunctions : NSObject
+(SharedFunctions *) sharedInstance;
-(UIViewController*)topMostController;
-(void)getUsrInfo;
-(void)saveUserData:(NSDictionary *)usrInfo;
-(void)clearUsrAndPass;
-(NSString *)formatDateToWS:(NSDate *)date ;
-(void)saveUser:(NSString *)usr andPass:(NSString *)pass;
-(void)getUsrAndPass;
-(NSString *)getCurrentStoryboard;
-(NSArray *)arrayByReplacingNullsWithBlanks  : (NSArray *) array;
-(NSMutableDictionary *) dictionaryByReplacingNullsWithStrings : (NSMutableDictionary *) dictionary;
-(NSString *)getDateFormatToWS;
-(void)updatePushToken:(NSString *)token;
-(void)showAlertWithMessage:(NSString *)message;
-(BOOL)addItemToCart:(NSMutableDictionary *)dictItem ofShop:(NSMutableDictionary *)dictShop;
-(void)deleteItemToCartAtIndex:(int)index;
-(void)updateItemToCart:(NSMutableDictionary *)dictItem;
-(void)setBadgeCart:(int)number;
-(void)goToCart;
@end
