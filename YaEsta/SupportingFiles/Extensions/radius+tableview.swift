//
//  radius+tableview.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 03/07/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }

}
