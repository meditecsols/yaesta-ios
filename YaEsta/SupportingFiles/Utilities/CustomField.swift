//
//  CustomField.swift
//  YaEsta
//
//  Created by Arturo Escamilla on 27/06/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import Foundation
import UIKit

class CustomField: UITextField {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds,
                                     UIEdgeInsetsMake(0, 15, 0, 15))
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds,
                                     UIEdgeInsetsMake(0, 15, 0, 15))
    }
    
    
}
